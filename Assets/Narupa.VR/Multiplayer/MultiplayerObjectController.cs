// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.Multiplayer
{
    
    /// <summary>
    ///     General thing (headset, controller) which could either be local or controlled by another player
    /// </summary>
    public class MultiplayerObjectController : MonoBehaviour
    {
        public int playerId = -1;

        public delegate void PlayerIdUpdatedHandler(int id);

        public event PlayerIdUpdatedHandler PlayerIdUpdated ;
        
        public void SetPlayerId(int id)
        {
            if (this.playerId == id)
                return;
            this.playerId = id;
            PlayerIdUpdated?.Invoke(this.playerId);
        }
    }
}