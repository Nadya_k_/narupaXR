﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.Utility.Unity;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR;

namespace Narupa.VR.Multiplayer
{
    /// <summary>
    /// Class for calibrating multiplayer Room-Scale SteamVR.
    /// </summary>
    public class MultiplayerSteamVrCalibration : MonoBehaviour
    {
        public readonly static PhysicallyCalibratedSpace PhysicalSpace = new PhysicallyCalibratedSpace();

        public static Matrix4x4 MultiplayerTRS { get; private set; } = Matrix4x4.Translate(Vector3.zero);

        /// <summary>
        /// Transform of narupa root object.
        /// </summary>
        public Transform NarupaTransform;

        private Dictionary<ulong, Vector3> lastKnownLighthousePositions
            = new Dictionary<ulong, Vector3>();

        public static void SetBoxRelativeTRS(Matrix4x4 trsMatrix)
        {
            MultiplayerTRS = trsMatrix;
        }

        public void SetBoxWorldTRS(Matrix4x4 trsMatrix)
        {
            MultiplayerTRS = PhysicalSpace.TransformMatrixWorldToLocal(trsMatrix);
        }

        private List<XRNodeState> nodeStates = new List<XRNodeState>();

        /// <summary>
        /// Calibrate the PhysicalSpace with the two TrackingReference devices
        /// with the lowest uniqueIDs.
        /// </summary>
        private void CalibrateFromLighthouses()
        {
            InputTracking.GetNodeStates(nodeStates);

            foreach (var node in nodeStates)
            {
                Vector3 position;

                if (node.nodeType == XRNode.TrackingReference && node.TryGetPosition(out position))
                    lastKnownLighthousePositions[node.uniqueID] = position;
            }

            var referencePositions = 
                lastKnownLighthousePositions
                .OrderBy(pair => pair.Key)
                .Select(pair => pair.Value)
                .ToList();

            if (referencePositions.Count >= 2)
            {
                PhysicalSpace.CalibrateWithTwoPoints(referencePositions[0], referencePositions[1]);
            }
            else
            {
                Debug.LogWarning($"Couldn't calibrate from lighthouses: need two valid reference positions.");
            }
        }

        /// <summary>
        /// Reposition the simulation box so it's forced to match the stored
        /// transform parameters.
        /// </summary>
        private void RepositionSimulationBox()
        {
            var trs = PhysicalSpace.TransformMatrixLocalToWorld(MultiplayerTRS);
            NarupaTransform.SetTRSFromMatrix(trs);
        }
        
        private void Update()
        {
            CalibrateFromLighthouses();
            RepositionSimulationBox();
        }
    }
}