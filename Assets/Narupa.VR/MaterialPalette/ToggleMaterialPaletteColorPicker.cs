﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.MaterialPalette
{
    /// <summary>
    ///     Uses the <see cref="MaterialPalette" /> to choose appropriate colors for a toggle.
    /// </summary>
    public class ToggleMaterialPaletteColorPicker : MaterialPaletteUser
    {
        private Toggle button;
        private ColorBlock colorBlock;
        private TextMeshProUGUI text;

        private readonly Color transparent = new Color(0, 0, 0, 0);

        [SerializeField] private bool transparentWhenDisabled;

        // Use this for initialization
        private void Start()
        {
            UpdateColors();
            button.onValueChanged.AddListener(value => OnToggleValueChange(value));
        }


        /// <inheritdoc />
        public override void UpdateColors()
        {
            button = GetComponent<Toggle>();
            var palette = GetPalette();
            if (palette == null)
                return;
            text = GetComponentInChildren<TextMeshProUGUI>();
            if (text != null) text.color = palette.PrimaryTextColor;

            colorBlock = button.colors;
            if (transparentWhenDisabled)
                colorBlock.normalColor = transparent;
            else
                colorBlock.normalColor = palette.PrimaryColor;
            colorBlock.highlightedColor = palette.LightPrimaryColor;
            colorBlock.disabledColor = palette.DividerColor;
            colorBlock.pressedColor = palette.DarkPrimaryColor;
            //Ensure we set the right initial colours.
            OnToggleValueChange(button.isOn);
        }

        private void Reset()
        {
            UpdateColors();
        }

        /// <summary>
        ///     Update colors when toggle changes value.
        /// </summary>
        /// <param name="value"></param>
        public void OnToggleValueChange(bool value)
        {
            var palette = GetPalette();
            //It is possible for this to be called before color picker initialised.
            if (button == null)
            {
                button = GetComponent<Toggle>();
                colorBlock = button.colors;
            }

            if (value)
            {
                colorBlock.normalColor = palette.SecondaryColor;
                colorBlock.highlightedColor = palette.LightSecondaryColor;
                colorBlock.pressedColor = palette.DarkSecondaryColor;
                button.colors = colorBlock;
            }
            else
            {
                if (transparentWhenDisabled)
                    colorBlock.normalColor = transparent;
                else
                    colorBlock.normalColor = palette.PrimaryColor;
                colorBlock.highlightedColor = palette.LightPrimaryColor;
                colorBlock.pressedColor = palette.DarkPrimaryColor;
                button.colors = colorBlock;
            }
        }
    }
}