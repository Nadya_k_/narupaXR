﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.MaterialPalette
{
    /// <summary>
    ///     Uses the <see cref="MaterialPalette" /> to choose appropriate colors for a button.
    /// </summary>
    public class ButtonMaterialPaletteColorPicker : MaterialPaletteUser
    {
        private Button button;
        private TextMeshProUGUI text;

        [SerializeField] private bool useAlternateColorOnHighlight;

        [SerializeField] private bool useSecondaryColor;

        // Use this for initialization
        private void Start()
        {
            UpdateColors();
        }


        /// <inheritdoc />
        public override void UpdateColors()
        {
            var buttons = GetComponentsInChildren<Button>();
            foreach (var button in buttons)
                UpdateColors(button);
        }
        
        private void UpdateColors(Button button)
        {
            var palette = GetPalette();
           
            if (button == null)
                return;
            if (palette == null)
                return;
            
            text = button.gameObject.GetComponentInChildren<TextMeshProUGUI>();
            if (text != null) text.color = palette.PrimaryTextColor;
            
            var colorBlock = button.colors;


            colorBlock.disabledColor = palette.DividerColor;
            if (!useSecondaryColor)
            {
                colorBlock.normalColor = palette.PrimaryColor;
                if (useAlternateColorOnHighlight)
                {
                    colorBlock.highlightedColor = palette.LightSecondaryColor;
                    colorBlock.pressedColor = palette.DarkSecondaryColor;
                }
                else
                {
                    colorBlock.highlightedColor = palette.LightPrimaryColor;
                    colorBlock.pressedColor = palette.DarkPrimaryColor;
                }
            }
            else
            {
                colorBlock.normalColor = palette.SecondaryColor;
                if (useAlternateColorOnHighlight)
                {
                    colorBlock.highlightedColor = palette.LightPrimaryColor;
                    colorBlock.pressedColor = palette.DarkPrimaryColor;
                }
                else
                {
                    colorBlock.highlightedColor = palette.LightSecondaryColor;
                    colorBlock.pressedColor = palette.DarkSecondaryColor;
                }
            }

            button.colors = colorBlock;
        }

       
        
    }

}