﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Nano.Science;
using Narupa.VR.Player.Control;
using NSB.MMD.RenderingTypes;
using NSB.Simbox.Topology;
using TMPro;
using UnityEngine;

namespace Narupa.VR.UI.Tooltips
{
    /// <summary>
    ///     Tooltip for displaying info about an atom.
    /// </summary>
    [RequireComponent(typeof(ObjectTooltip))]
    public class ProteinAtomTooltip : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI atomElementText;

        [SerializeField] private Transform atomFollower;

        [SerializeField] private TextMeshProUGUI atomIndexText;
        [SerializeField] private TextMeshProUGUI atomNameText;

        private TopologyAtom currentAtom;

        [SerializeField] private FrameSource frameSource;

        private bool initialised;

        private ObjectTooltip lineDrawer;
        private VrAppController player;
        [SerializeField] private TextMeshProUGUI residueIdText;
        [SerializeField] private TextMeshProUGUI residueNameText;

        private Topology topology;

        public void SetAtom(TopologyAtom atom)
        {
            currentAtom = atom;
            if (atom == null)
                return;
            if (initialised == false) Awake();
            var residue = atom as ProteinAtom;
            if (residue != null)
            {
                residueNameText.text = "Residue Name: " + residue.ResidueName;
                if (residue.ResidueId != 0)
                    residueIdText.text = "Residue ID: " + residue.ResidueId;
                else
                    residueIdText.text = "";
                atomNameText.text = "Atom Name: " + residue.Name;
                atomIndexText.text = "Atom Index: " + residue.Index;
                atomElementText.text = "Element: " + GetElementSymbolForAtom(residue);
            }
            else
            {
                residueNameText.text = "";
                residueIdText.text = "";
                atomNameText.text = "Atom Name: " + atom.Name;
                atomIndexText.text = "Atom Index: " + atom.Index;
                atomElementText.text = GetElementSymbolForAtom(atom);
            }
        }

        private string GetElementSymbolForAtom(TopologyAtom atom)
        {
            if (topology.AtomToVisibleIndexMap.ContainsKey(atom))
                try
                {
                    return PeriodicTable.GetElementProperties((Element) player.Playback.LastFullFrame
                        .AtomTypes[topology.AtomToVisibleIndexMap[atom]]).Symbol;
                }
                catch (IndexOutOfRangeException)
                {
                    return string.Empty;
                }

            return string.Empty;
        }

        // Use this for initialization
        private void Awake()
        {
            lineDrawer = GetComponent<ObjectTooltip>();
            lineDrawer.DrawLineFrom = transform;
            lineDrawer.DrawLineTo = atomFollower;
            player = FindObjectOfType<VrAppController>();
            if (player.Playback.Topology != null)
                topology = player.Playback.Topology;
            else
                return;
            initialised = true;
        }

        private void Update()
        {
            if (currentAtom != null)
                atomFollower.localPosition =
                    frameSource.Component.Frame.AtomPositions[topology.AtomToVisibleIndexMap[currentAtom]];
        }
    }
}