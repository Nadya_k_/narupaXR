﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using TMPro;
using UnityEngine;

namespace Narupa.VR.UI.Radial
{
	public class RadialSliderLabel : MonoBehaviour {

		private TextMeshProUGUI valueText;

		[SerializeField] private RadialSlider radialSlider;

		[SerializeField] private int decimalPlaces;
		[SerializeField] private string suffix;

		private void RadialSliderOnValueChanged(object sender, RadialSlider.SliderValueChangeEventArgs e)
		{
			SetValue(e.Value);
		}

		private void SetValue(float value)
		{
			if(decimalPlaces == 1)
				valueText.text = string.Format("{0:F1}", value) + suffix;
			else
				valueText.text = $"{value}" + suffix;
		}

		private void OnEnable()
		{
			valueText = GetComponentInChildren<TextMeshProUGUI>();
			if (radialSlider == null)
			{
				radialSlider = GetComponent<RadialSlider>();
				radialSlider.ValueChanged += RadialSliderOnValueChanged;
			}
			SetValue(radialSlider.CurrentValue);
		}
	}
}
