﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using UnityEngine;

namespace Narupa.VR.UI
{
    /// <summary>
    ///     Class for making an object float in front of player.
    /// </summary>
    public class ObjectToCameraMover : MonoBehaviour
    {
        [SerializeField]
        [Tooltip(
            "Whether this object should be set to a specific height, if false Object Height is used as a relative value.")]
        private bool constantHeight = true;

        [SerializeField] private float distance = 2f;

        [SerializeField] private bool followPlayer;

        private Quaternion localRotation;

        [SerializeField] private float objectHeight = 1.5f;

        [SerializeField] [Range(-180f, 180f)] private float rotationOffset;

        [SerializeField] private float rotationSpeed = 5f;

        [SerializeField] private bool snap = true;

        [SerializeField] private float translationSpeed = 3f;

        private void Awake()
        {
            localRotation = transform.localRotation;
        }

        private void OnEnable()
        {
            //var scale = transform.localScale;
            //transform.localScale = 0.000001f * Vector3.one;
            //iTween.ScaleTo(gameObject, scale, 0.5f);

            try
            {
                MoveInFrontOfCamera(Camera.main.transform.position, Camera.main.transform.forward, true);
            }
            catch (NullReferenceException)
            {
                Debug.Log("No camera to move to.");
            }
        }

        private void MoveInFrontOfCamera(Vector3 cameraPosition, Vector3 direction, bool snapTo = false)
        {
            var finalDirection = Quaternion.AngleAxis(rotationOffset, Vector3.up) * direction;
            var destination = cameraPosition + distance * finalDirection;
            if (!constantHeight) destination += new Vector3(0, objectHeight, 0);
            if (snapTo) transform.position = destination;
            else
                transform.position = Vector3.Lerp(transform.position, destination, translationSpeed * Time.deltaTime);
            if (constantHeight)
                transform.position = new Vector3(transform.position.x, objectHeight, transform.position.z);

            //Rotate the object to look at the player.
            var relativePos = transform.position - cameraPosition;
            relativePos.y = 0f;
            var rotation = Quaternion.LookRotation(relativePos) * localRotation;
            if (snapTo) transform.rotation = rotation;
            else transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
        }

        private void Update()
        {
            if (followPlayer) MoveInFrontOfCamera(Camera.main.transform.position, Camera.main.transform.forward, snap);
        }
    }
}