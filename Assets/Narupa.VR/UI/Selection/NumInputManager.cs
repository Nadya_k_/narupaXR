﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.VR.UI.Selection
{
    public class NumInputManager : MonoBehaviour
    {
        private ResidueButtonManager residueButtonManager;
        [SerializeField] private GameObject resultsBox;
        [SerializeField] private GameObject scrollView;

        public static string InputValue { get; private set; }

        // Use this for initialization
        private void Awake()
        {
            ResetInputValue();
            residueButtonManager = scrollView.GetComponent<ResidueButtonManager>();
        }

        public void UpdateInputValue(string newInput)
        {
            if (resultsBox != null)
            {
                //Code to update search text
                if (newInput == "Backspace")
                {
                    if (InputValue.Length > 0) InputValue = InputValue.Remove(InputValue.Length - 1);
                }
                else if (newInput == "Clear")
                {
                    ResetInputValue();
                }
                else
                {
                    if (InputValue.Length < 9) InputValue += newInput;
                }

                if (InputValue == "")
                {
                    resultsBox.SetActive(false);
                }
                else
                {
                    resultsBox.SetActive(true);
                    residueButtonManager.UpdateResidueData(InputValue);
                }
            }
        }

        public static void ResetInputValue()
        {
            InputValue = "";
        }
    }
}