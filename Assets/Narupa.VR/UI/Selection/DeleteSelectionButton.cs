﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Selection;

namespace Narupa.VR.UI.Selection
{
    public class DeleteSelectionButton : VrButtonBase
    {
        /// <summary>
        ///     The selection associated with this button.
        /// </summary>
        public InteractiveSelection Selection;

        /// <summary>
        ///     Event for delete selection clicked.
        /// </summary>
        public event EventHandler DeleteSelectionClicked;

        protected override void Awake()
        {
            base.Awake();
            ButtonClicked += OnButtonClicked;
        }

        private void OnButtonClicked(object sender, EventArgs e)
        {
            if (DeleteSelectionClicked != null) DeleteSelectionClicked(Selection, null);
        }
    }
}