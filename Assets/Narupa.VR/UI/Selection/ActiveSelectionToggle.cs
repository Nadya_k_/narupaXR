﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.VR.Selection;
using TMPro;
using UnityEngine.EventSystems;

namespace Narupa.VR.UI.Selection
{
    /// <summary>
    ///     Toggle for browsing the list of selections.
    /// </summary>
    public class ActiveSelectionToggle : VrToggleBase
    {
        /// <summary>
        ///     Handler for a selection being selected/unselected.
        /// </summary>
        /// <param name="selection"></param>
        /// <param name="e"></param>
        public delegate void ActiveSelectionHandler(InteractiveSelection selection, EventArgs e);

        private TextMeshProUGUI buttonText;

        /// <summary>
        ///     The selection associated with this button.
        /// </summary>
        public InteractiveSelection Selection { get; private set; }

        /// <summary>
        ///     Event for when the selection button is highlighted.
        /// </summary>
        public event ActiveSelectionHandler SelectionHighlighted;

        /// <summary>
        ///     Event for when the selection button is unhighlighted.
        /// </summary>
        public event ActiveSelectionHandler SelectionUnhighlighted;

        /// <summary>
        ///     Event for when the button is clicked, and a selection is chosen.
        /// </summary>
        public event ActiveSelectionHandler SelectionChosen;

        public override void OnPointerEnter(PointerEventData eventData)
        {
            if (SelectionHighlighted != null) SelectionHighlighted(Selection, null);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            if (SelectionUnhighlighted != null) SelectionUnhighlighted(Selection, null);
        }

        public void SetSelection(InteractiveSelection selection, bool isActiveSelection = false)
        {
            if (Selection != null) selection.SelectedAtomsChanged -= OnSelectedAtomsChanged;
            Selection = selection;
            if (Toggle == null) Awake();
            Toggle.isOn = isActiveSelection;
            Toggle.onValueChanged.Invoke(Toggle.isOn);
            if (buttonText == null) buttonText = GetComponentInChildren<TextMeshProUGUI>();
            buttonText.text = $"{selection.Name}: {selection.Selection.Count} atoms.";
            selection.SelectedAtomsChanged += OnSelectedAtomsChanged;
        }

        private void OnSelectedAtomsChanged()
        {
            buttonText.text = $"{Selection.Name}: {Selection.Selection.Count} atoms.";
        }

        // Use this for initialization
        private void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
        }

        protected override void Awake()
        {
            base.Awake();
            buttonText = GetComponentInChildren<TextMeshProUGUI>();
            ToggleChangeValue += OnToggleChangeValue;
        }

        private void OnToggleChangeValue(object sender, EventArgs e)
        {
            var selected = (bool) sender;
            if (selected) SelectionChosen?.Invoke(Selection, null);
        }
    }
}