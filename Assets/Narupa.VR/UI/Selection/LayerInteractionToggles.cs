﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Nano.Transport.Variables.Interaction;
using Narupa.VR.Selection;
using UnityEngine;
using UnityEngine.UI;

namespace Narupa.VR.UI.Selection
{
    public class LayerInteractionToggles : MonoBehaviour
    {
        [SerializeField] private Toggle groupInteractionToggle;

        [SerializeField] private Toggle noInteraction;

        private SelectionManager selectionManager;

        [SerializeField] private Toggle singleAtomToggle;

        [SerializeField] private Toggle restraintToggle; 
        
        
        private void Awake()
        {
            selectionManager = FindObjectOfType<SelectionManager>();
            selectionManager.ActiveSelectionChanged += OnSelectionChanged;
        }


        private void OnEnable()
        {
            if (selectionManager == null)
            {
                selectionManager = FindObjectOfType<SelectionManager>();
                selectionManager.ActiveSelectionChanged += OnSelectionChanged;
            }

            OnSelectionChanged(selectionManager.ActiveSelection, null);
        }

        private void OnSelectionChanged(object sender, EventArgs e)
        {
            var selection = sender as InteractiveSelection;
            if (selection == null)
            {
                groupInteractionToggle.gameObject.SetActive(false);
                restraintToggle.gameObject.SetActive(false);
                if (ActiveSelectionTemplate.InteractWithBaseLayer)
                {
                    singleAtomToggle.isOn = true;
                    singleAtomToggle.onValueChanged.Invoke(true);
                }
                else
                {
                    noInteraction.isOn = true;
                    noInteraction.onValueChanged.Invoke(true);
                }
            }
            else
            {
                groupInteractionToggle.gameObject.SetActive(true);
                restraintToggle.gameObject.SetActive(true);
                
                switch (selection.InteractionProperties.InteractionType)
                {
                    case InteractionType.NoInteraction:
                        if (selection.InteractionProperties.RestraintLayer)
                            restraintToggle.isOn = true;
                        else
                            noInteraction.isOn = true;
                        break;
                    case InteractionType.AllAtom:
                        groupInteractionToggle.isOn = true;
                        break;
                    case InteractionType.SingleAtom:
                        singleAtomToggle.isOn = true;
                        break;
                    case InteractionType.NearestAtomUpdate:
                        singleAtomToggle.isOn = true;
                        break;
                    case InteractionType.Group:
                        groupInteractionToggle.isOn = true;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}