﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Narupa.VR.UI.Selection
{
    public class SelectionEditorButton : VrButtonBase
    {
        /// <summary>
        ///     Event for new selection button click.
        /// </summary>
        public event EventHandler SelectionEditorClicked;

        protected override void Awake()
        {
            base.Awake();
            ButtonClicked += OnButtonClicked;
        }

        private void OnButtonClicked(object sender, EventArgs e)
        {
            if (SelectionEditorClicked != null) SelectionEditorClicked(this, null);
        }
    }
}