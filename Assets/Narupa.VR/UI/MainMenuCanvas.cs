﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Narupa.VR.UI
{
    /// <summary>
    ///     Placeholder used to make finding the root main menu canvas easy.
    /// </summary>
    public class MainMenuCanvas : VrCanvas
    {
    }
}