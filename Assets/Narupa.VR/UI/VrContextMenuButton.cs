﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using UnityEngine;

namespace Narupa.VR.UI
{
    /// <summary>
    ///     Class for handling button press for switching between canvases.
    /// </summary>
    internal class VrContextMenuButton : VrButtonBase
    {
        [SerializeField] private VrCanvas canvasToSwitchTo;

        [SerializeField] private VrContextMenu contextMenu;

        protected override void Awake()
        {
            base.Awake();
            ButtonClicked += OnButtonClicked;
        }

        private void OnButtonClicked(object sender, EventArgs e)
        {
            contextMenu.SwitchToCanvas(canvasToSwitchTo);
        }
    }
}