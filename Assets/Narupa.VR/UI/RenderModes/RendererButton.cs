﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using NSB.MMD.Base;
using NSB.Utility;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Narupa.VR.UI.RenderModes
{
    /// <summary>
    ///     Class for selecting the active renderer.
    /// </summary>
    public class RendererButton : InstanceView<MMDRenderer>
    {
        private Button button;

        private RendererManager rendererManager;
        private TextMeshProUGUI text;

        public event EventHandler RendererSelected;

        private void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(OnRendererSelected);
            text = GetComponentInChildren<TextMeshProUGUI>();
        }

        private void OnRendererSelected()
        {
            rendererManager.SetRenderer(config);
            RendererSelected?.Invoke(config, null);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            rendererManager?.SetRenderer(config);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            rendererManager.SwitchToPreviousRenderer();
        }

        protected override void Configure()
        {
            //Get the renderer selection in the parent so we can show renderer when highlighted.
            rendererManager = config.GetComponentInParent<RendererManager>();
            //Set button text to match renderer name.
            text.text = config.name;
        }
    }
}