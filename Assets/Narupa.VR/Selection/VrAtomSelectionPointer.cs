﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.Client.UI.Pointer;
using Narupa.VR.Player.Control;
using Narupa.VR.UI.Pointer;
using NSB.Examples.Player.Control;
using NSB.Interaction;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;
using Valve.VR;

namespace Narupa.VR.Selection
{
    /// <summary>
    ///     Detects when an atom has been touched.
    /// </summary>
    /// <remarks>
    ///     When in Selection Mode, used to trigger highlighting of atom to select.
    /// </remarks>
    public class VrAtomSelectionPointer : MonoBehaviour
    {
        public delegate void AtomColliderHandler(AtomColliderEventInfo info);

        private int atomCurrentlyColliding = -1;

        private bool colliding;

        private uint controllerId;

        /// <summary>
        ///     Cutoff to be used for detecting collision with atom.
        /// </summary>
        [SerializeField] [Tooltip("Cutoff to be used for detecting collision with atom.")]
        private float cutOff;

        private UiPointerInputModule inputModule;

        private VrPlaybackRenderer playback;

        private WorldLocalSpaceTransformer relativeRootTransform;

        public SteamVR_Input_Sources inputSource;

        /// <summary>
        ///     Event triggered when the transform this instance is attached to touches an atom.
        /// </summary>
        public event AtomColliderHandler TouchedAtom;

        /// <summary>
        ///     Event triggered when the transform this instance is attached to stops touching an atom.
        /// </summary>
        public event AtomColliderHandler UntouchedAtom;

        private bool CheckCollisionWithAtom(NSBFrame frame, out int nearestAtom)
        {
            float distSqr;
            var localPos = relativeRootTransform.TransformWorldSpacePointToLocalSpace(transform.position);
            var minDistSqr = float.MaxValue;
            nearestAtom = -1;
            for (var i = 0; i < frame.AtomCount; i++)
            {
                distSqr = FasterMath.Sub(frame.AtomPositions[i], localPos).sqrMagnitude;
                //distSqr = Vector3.Distance(transformPos, frame.AtomPositions[i]);
                if (distSqr < minDistSqr)
                {
                    nearestAtom = i;
                    minDistSqr = distSqr;
                }
            }

            //check whether actually collided.
            if (minDistSqr < 0.75f * ExampleMolecularStyle.ElementRadiuses[frame.AtomTypes[nearestAtom]] + cutOff)
                return true;
            return false;
        }

        private void OnCollideWithAtom(int atomId)
        {
            controllerId = (uint) this.inputSource;
            var eventInfo = new AtomColliderEventInfo();
            eventInfo.AtomId = atomId;
            eventInfo.ColliderId = controllerId;
            if (TouchedAtom != null) TouchedAtom(eventInfo);
        }

        private void OnExitAtomCollision(int atomId)
        {
            controllerId = (uint) this.inputSource;
            var eventInfo = new AtomColliderEventInfo();
            eventInfo.AtomId = atomId;
            eventInfo.ColliderId = controllerId;
            if (UntouchedAtom != null) UntouchedAtom(eventInfo);
        }

        private void OnTriggerEnter(Collider collision)
        {
            var atomInfo = collision.gameObject.GetComponent<AtomColliderInfo>();
            if (atomInfo != null)
            {
                controllerId = (uint) this.inputSource;
                var eventInfo = new AtomColliderEventInfo();
                eventInfo.AtomId = atomInfo.AtomID;
                eventInfo.ColliderId = atomInfo.ColliderID;
                if (TouchedAtom != null) TouchedAtom(eventInfo);
            }
        }

        private void OnTriggerExit(Collider collision)
        {
            var atomInfo = collision.gameObject.GetComponent<AtomColliderInfo>();
            if (atomInfo != null)
            {
                atomInfo.ColliderID = controllerId;
                var eventInfo = new AtomColliderEventInfo();
                eventInfo.AtomId = atomInfo.AtomID;
                eventInfo.ColliderId = atomInfo.ColliderID;
                if (UntouchedAtom != null) UntouchedAtom(eventInfo);
            }
        }

        private void Start()
        {
            playback = FindObjectOfType<VrPlaybackRenderer>();
            relativeRootTransform = FindObjectOfType<WorldLocalSpaceTransformer>();
            inputModule = FindObjectOfType<UiPointerInputModule>();
        }

        private void Update()
        {
            //Only update collisions if we have frames,
            if (playback.Ready)
            {
                var nearestAtom = -1;
                bool inAtom;
                //and not if the laser pointer is over a UI element.
                if (inputModule != null && inputModule.isPointerActive)
                    inAtom = false;
                else
                    inAtom = CheckCollisionWithAtom(playback.LastFullFrame,
                        out nearestAtom);

                //If a new collision, fire atom collision event.
                if (!colliding && inAtom)
                {
                    atomCurrentlyColliding = nearestAtom;
                    colliding = true;
                    OnCollideWithAtom(nearestAtom);
                }
                else if (inAtom)
                {
                    //If the nearest atom isn't the one this instance was previously colliding with, stop colliding with the previous atom and collide with the new atom.
                    if (colliding && nearestAtom != atomCurrentlyColliding)
                    {
                        OnExitAtomCollision(atomCurrentlyColliding);
                        atomCurrentlyColliding = nearestAtom;
                        colliding = true;
                        OnCollideWithAtom(nearestAtom);
                    }
                }
                //If not near an atom and we were colliding before, then fire collision exit event.
                else if (colliding)
                {
                    OnExitAtomCollision(atomCurrentlyColliding);
                    atomCurrentlyColliding = -1;
                    colliding = false;
                }
            }
        }
    }
}