﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Narupa.Client.UI.Pointer;
using Narupa.VR.UI.Pointer;
using UnityEngine;

namespace Narupa.VR.Controller
{
	
	/// <summary>
	/// 	Manages the current ControllerMode, with event callbacks for when modes start and end
	/// </summary>
	public class VrControllerStateManager : MonoBehaviour, IUiPointerOverride
	{

		[SerializeField] private ControllerMode activeMode = null;

		/// <summary>
		/// 	The currently active mode
		/// </summary>
		public ControllerMode ActiveMode
		{
			get { return activeMode; }
		}
		
		/// <summary>
		/// 	List of possible modes
		/// </summary>
		public List<ControllerMode> Modes = new List<ControllerMode>();

		public List<ControllerMode> DisabledModes = new List<ControllerMode>();

		private void Start()
		{
			/// Sets the initial mode to the first that appears in Modes
			if(Modes.Count > 0 && ActiveMode == null)
				SetMode(Modes[0]);
			
			// Add override to prevent UI pointer from overriding an active cursor (interaction, selection)
			FindObjectOfType<UiPointerInputModule>().AddPointerOverride(this);
		}

		/// <summary>
		/// 	Event for when the controller mode changes
		/// </summary>
		public class ControllerModeChangedEventArgs : EventArgs
		{
			public readonly ControllerMode OldMode;
			public readonly ControllerMode NewMode;

			public ControllerModeChangedEventArgs(ControllerMode oldMode, ControllerMode newMode)
			{
				this.OldMode = oldMode;
				this.NewMode = newMode;
			}
		}
		
		public class ControllerModeAlteredEventArgs : EventArgs
		{
			public readonly ControllerMode Mode;

			public ControllerModeAlteredEventArgs(ControllerMode mode)
			{
				this.Mode = mode;
			}
		}

		/// <summary>
		/// 	Called when a mode is being replaced
		/// </summary>
		public event EventHandler<ControllerModeChangedEventArgs> ModeEnd;
		
		/// <summary>
		/// 	Called when a mode is beginning
		/// </summary>
		public event EventHandler<ControllerModeChangedEventArgs> ModeStart;

		public event EventHandler<ControllerModeAlteredEventArgs> ModeEnabled;
		public event EventHandler<ControllerModeAlteredEventArgs> ModeDisabled;

		/// <summary>
		/// 	Change the current active mode
		/// </summary>
		/// <param name="mode"></param>
		public void SetMode(ControllerMode mode)
		{
			if (activeMode == mode)
				return;
			ModeEnd?.Invoke(this, new ControllerModeChangedEventArgs(ActiveMode, mode));
			var oldMode = this.ActiveMode;
			this.activeMode = mode;
			ModeStart?.Invoke(this, new ControllerModeChangedEventArgs(oldMode, ActiveMode));
		}
		
		/// <summary>
		/// 	Changes the current active mode to the mode with the specified name 
		/// </summary>
		/// <param name="name"></param>
		public void SetMode(string name)
		{
			var mode = this.Modes.FirstOrDefault(m => m.name == name);
			if(mode != null)
				SetMode(mode);
		}

		/// <summary>
		/// 	Resets the mode to the default
		/// </summary>
		public void SetModeDefault()
		{
			SetMode(this.Modes[0]);
		}

		public void EnableMode(string name)
		{
			var disabledMode = this.DisabledModes.FirstOrDefault(m => m.name == name);
			if (disabledMode != null)
			{
				DisabledModes.Remove(disabledMode);
				Modes.Add(disabledMode);
				ModeEnabled?.Invoke(this, new ControllerModeAlteredEventArgs(disabledMode));
			}
		}

		public void DisableMode(string name)
		{
			var enabledMode = this.Modes.FirstOrDefault(m => m.name == name);
			if (enabledMode != null)
			{
				Modes.Remove(enabledMode);
				DisabledModes.Add(enabledMode);
				ModeDisabled?.Invoke(this, new ControllerModeAlteredEventArgs(enabledMode));
			}
		}

		public List<GameObject> activeCursors = new List<GameObject>();
		
		/// <summary>
		/// Is the cursor of the controller being used to select or interact
		/// </summary>
		public bool CursorActive => activeCursors.Count > 0;
		
		public void SetCursorActive(GameObject cursor, bool active)
		{
			bool activeCurrent = activeCursors.Contains(cursor);
			if (activeCurrent && !active)
				activeCursors.Remove(cursor);
			else if(!activeCurrent && active)
				activeCursors.Add(cursor);
		}

		public bool IsUiPointerActive => !CursorActive;
	}
}
