﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using NSB.MMD;
using NSB.MMD.Base;
using UnityEngine;

namespace Narupa.VR.Renderers
{
    /// <summary>
    /// Renders atoms with trails.
    /// </summary>
    public class NsbWow : MMDRenderer
    {
        [SerializeField] private PointsRepresentation atomOutlines;

        [SerializeField] private LinesRepresentation bondOutlines;

        [Header("Setup")] [SerializeField] private LiquoriceRepresentation liquoriceRepresentation;

        /// <summary>
        /// Settings for this renderer.
        /// </summary>
        public NsbConfig Settings = new NsbConfig();

        [SerializeField] private AtomTrailRenderer trailRenderer;

        public override RendererConfig Config => Settings;

        /// <inheritdoc />
        public override void Refresh()
        {
            liquoriceRepresentation.Frame = Frame;
            liquoriceRepresentation.Selection = Selection;
            liquoriceRepresentation.Style = Style;

            liquoriceRepresentation.Refresh();

            trailRenderer.Frame = Frame;
            trailRenderer.Selection = Selection;
            trailRenderer.Style = Style;

            atomOutlines.Frame = Frame;
            atomOutlines.Selection = Selection;

            bondOutlines.Frame = Frame;
            bondOutlines.Selection = Selection;

            if (Settings.ShowOutlines.Value)
            {
                atomOutlines.Refresh();
                bondOutlines.Refresh();
            }
        }

        /// <inheritdoc />
        public override float GetAtomBoundingRadius(int id)
        {
            if (!atomOutlines.Valid)
                return 0;
            return atomOutlines.GetAtomBoundingRadius(id);
        }

        /// <inheritdoc />
        [Serializable]
        public class NsbConfig : RendererConfig
        {
            /// <summary>
            /// Whether to show outlines.
            /// </summary>
            public BoolParameter ShowOutlines = new BoolParameter();

            /// <inheritdoc />
            protected override void Setup()
            {
                ShowOutlines = AddBool("Show Outlines", true);
            }
        }
    }
}