﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.Client.Network;
using Narupa.VR.Multiplayer;
using UnityEngine;

namespace Narupa.VR
{
    /// <summary>
    ///     Handles transformations and scaling of the simulation box in world space.
    /// </summary>
    /// <remarks>
    ///     Should be placed in a child game object of the simulation playback game object.
    /// </remarks>
    public class WorldLocalSpaceTransformer : MonoBehaviour
    {
        public MultiplayerSteamVrCalibration calibration;

        private Transform parentTransform;

        private readonly Matrix4x4?[] transformMatrices = new Matrix4x4?[2];

        private bool trsUpdated;
        private Vector3 startingCenter;
        private Vector3 startingCenterOffset;
        private Vector3 startingBoxTranslation;
        private float startingDistance;

        private Quaternion startingRotation;
        private Quaternion startingRotationOffsetInverse;
        private Vector3 startingScale;

        private new Transform transform;

        private OnePointTranslateRotateGesture onePointGesture;
        private TwoPointTranslateRotateScaleGesture twoPointGesture;

        /// <summary>
        ///     Event triggered when the scale changes in multiplayer.
        /// </summary>
        public event EventHandler MultiplayerScaleChange;

        private NetworkManager networkManager;

        private void Awake()
        {
            transform = base.transform;

            parentTransform = transform.parent;
            networkManager = FindObjectOfType<NetworkManager>();
        }
        
        /// <summary>
        ///     Transforms a point from world space (left-handed) into narupa space (right-handed).
        /// </summary>
        /// <param name="worldSpacePosition"></param>
        /// <returns></returns>
        public Vector3 TransformWorldSpacePointToLocalSpace(Vector3 worldSpacePosition)
        {
            // World Space (Left-Handed) to Simulation Space (Left-Handed)
            return transform.InverseTransformPoint(worldSpacePosition);
        }

        /// <summary>
        ///     Transforms a point from world space (left-handed) into narupa space (right-handed).
        /// </summary>
        /// <param name="worldSpacePosition"></param>
        /// <returns></returns>
        public Vector3 TransformWorldSpacePointToNarupaSpace(Vector3 worldSpacePosition)
        {
            // World Space (Left-Handed) to Simulation Space (Left-Handed)
            var pt = transform.InverseTransformPoint(worldSpacePosition);
            // Simulation Space (Left-Handed) to Narupa Space (Right-Handed)
            pt.z = -pt.z;
            return pt;
        }

        /// <summary>
        ///     Transforms a point from narupa space into world space.
        /// </summary>
        /// <param name="narupaSpacePosition"></param>
        /// <returns></returns>
        public Vector3 TransformNarupaSpacePointToWorldSpace(Vector3 narupaSpacePosition)
        {
            var pt = narupaSpacePosition;
            // Narupa Space (Right-Handed) to Simulation Space (Left-Handed)
            pt.z = -pt.z;
            // Simulation Space (Left-Handed) to World Space (Left-Handed)
            pt = transform.TransformPoint(pt);
            return pt;
        }
        
        /// <summary>
        ///     Transforms a point from narupa space into world space.
        /// </summary>
        /// <param name="narupaSpaceDirection"></param>
        /// <returns></returns>
        public Vector3 TransformNarupaSpaceDirectionToWorldSpace(Vector3 narupaSpaceDirection)
        {
            var direction = narupaSpaceDirection;
            // Narupa Space (Right-Handed) to Simulation Space (Left-Handed)
            direction.z = -direction.z;
            // Simulation Space (Left-Handed) to World Space (Left-Handed)
            direction = transform.TransformDirection(direction);
            return direction;
        }

        /// <summary>
        ///     Transforms a quaternion from wolrd space into narupa space.
        /// </summary>
        /// <param name="worldSpaceQuaternion"></param>
        /// <returns></returns>
        public Quaternion TransformWorldSpaceQuaternionToNarupaSpace(Quaternion worldSpaceQuaternion)
        {
            // World Space (Left-Handed) to Simulation Space (Left-Handed)
            var rot = Quaternion.Inverse(transform.rotation) * worldSpaceQuaternion;
            // Simulation Space (Left-Handed) to Narupa Space (Right-Handed)
            //rot.z = -rot.z;
            //rot.w = -rot.w;
            return rot;
        }

        /// <summary>
        ///     Transforms a quaternion from narupa space into world space.
        /// </summary>
        /// <param name="narupaSpaceQuaternion"></param>
        /// <returns></returns>
        public Quaternion TransformNarupaSpaceQuaternionToWorldSpace(Quaternion narupaSpaceQuaternion)
        {
            var rot = narupaSpaceQuaternion;
            // Narupa Space (Right-Handed) to Simulation Space (Left-Handed)
            //rot.z = -rot.z;
            //rot.w = -rot.w;
            // Simulation Space (Left-Handed) to World Space (Left-Handed)
            rot = transform.rotation * rot;
            return rot;
        }

        /// <summary>
        ///     Registers a point for performing transformations of the Narupa scene scaling.
        /// </summary>
        /// <param name="worldSpacePosition"></param>
        /// <param name="worldRotation"></param>
        /// <returns></returns>
        public int RegisterTransformPoint(Vector3 worldSpacePosition, Quaternion worldRotation)
        {
            var index = 0;
            for (; index < transformMatrices.Length; index++)
            {
                if (transformMatrices[index].HasValue) continue;

                transformMatrices[index] = Matrix4x4.TRS(worldSpacePosition, worldRotation, Vector3.one);

                break;
            }

            if (index < transformMatrices.Length)
            {
                var matrixes = new Matrix4x4[2];
                var v = 0;

                for (int i = 0; i < transformMatrices.Length; ++i)
                {
                    if (transformMatrices[i].HasValue == false) continue;

                    matrixes[v++] = transformMatrices[i].Value;

                    if (v == 2) break;
                }

                if (v == 2)
                {
                    onePointGesture = null;
                    twoPointGesture = new TwoPointTranslateRotateScaleGesture();
                    twoPointGesture.BeginGesture(MultiplayerSteamVrCalibration.PhysicalSpace.TransformMatrixLocalToWorld(MultiplayerSteamVrCalibration.MultiplayerTRS),
                                                 matrixes[0],
                                                 matrixes[1]);
                }
                else if (v == 1)
                {
                    twoPointGesture = null;
                    onePointGesture = new OnePointTranslateRotateGesture();
                    onePointGesture.BeginGesture(MultiplayerSteamVrCalibration.PhysicalSpace.TransformMatrixLocalToWorld(MultiplayerSteamVrCalibration.MultiplayerTRS), 
                                                 matrixes[0]);
                }
                else
                {
                    onePointGesture = null;
                    twoPointGesture = null;
                }

                return index;
            }

            throw new Exception("No index available");
        }

        /// <summary>
        ///     Updates a transform point.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="worldSpacePosition"></param>
        /// <param name="worldSpaceRotation"></param>
        public void UpdateTransformPoint(int index, Vector3 worldSpacePosition, Quaternion worldSpaceRotation)
        {
            transformMatrices[index] = Matrix4x4.TRS(worldSpacePosition, worldSpaceRotation, Vector3.one);
        }

        /// <summary>
        ///     Releases a previously added transform point.
        /// </summary>
        /// <param name="index"></param>
        public void ReleaseTransformPoint(int index)
        {
            transformMatrices[index] = null;

            startingCenter = parentTransform.position;

            onePointGesture = null;
            twoPointGesture = null;
        }

        private void LateUpdate()
        {
            var matrixes = new Matrix4x4[2];
                var v = 0;

                for (int i = 0; i < transformMatrices.Length; ++i)
                {
                    if (transformMatrices[i].HasValue == false) continue;

                    matrixes[v++] = transformMatrices[i].Value;

                    if (v == 2) break;
                }

            if (onePointGesture != null)
            {
                var matrix = onePointGesture.UpdateControlPoint(matrixes[0]);
                calibration.SetBoxWorldTRS(matrix);

                trsUpdated = true;
            }
            else if (twoPointGesture != null)
            {
                var matrix = twoPointGesture.UpdateControlPoints(matrixes[0], matrixes[1]);
                calibration.SetBoxWorldTRS(matrix);

                trsUpdated = true;
            }
            else if  (trsUpdated)
            {
                trsUpdated = false;

                networkManager?.CurrentConnection.SendMultiplayerBoxTRS(MultiplayerSteamVrCalibration.MultiplayerTRS);
            }
        }
    }
}