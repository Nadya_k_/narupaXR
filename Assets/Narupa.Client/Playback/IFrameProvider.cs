﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using Narupa.Client.Playback;
using NSB.Processing;

namespace Narupa.Client.Network
{
	/// <summary>
	/// Interface for something which provides a history of frames, either a pre made trajectory or a running simulation
	/// </summary>
	public interface IFrameProvider {

		/// <summary>
		/// Is there at least one frame, and hence the provider is ready for rendering
		/// </summary>
		bool IsFrameHistoryReady { get; }

		/// <summary>
		/// Event called each time the frame history is altered
		/// </summary>
		event Action FrameHistoryUpdated;

		/// <summary>
		/// The list of frames
		/// </summary>
		IFrameHistory<NSBFrame> History { get; }

		/// <summary>
		/// Is the history a set list of frames (and hence should loop) or a live simulation which is being updated
		/// </summary>
		bool IsFixedHistory { get; }

		/// <summary>
		/// Update function called in Unity
		/// </summary>
		void Update();

	}
}
