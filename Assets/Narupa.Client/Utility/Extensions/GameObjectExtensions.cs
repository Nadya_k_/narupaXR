using UnityEngine;

namespace Narupa.Client.Utility.Extensions
{
    public static class GameObjectExtensions
    {
        public static T GetOrCreateComponent<T>(this GameObject gameObject) where T : UnityEngine.Component
        {
            var found = gameObject.GetComponent<T>();
            return found != null ? found :  gameObject.AddComponent<T>();
        }
    }
}