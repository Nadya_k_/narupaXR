using UnityEngine;

namespace Narupa.Client.Utility.Extensions
{
    public static class MaterialExtensions
    {
        public static void SetKeyword(this Material material, string keyword, bool value = true)
        {
            if(value)
                material.EnableKeyword(keyword);
            else
                material.DisableKeyword(keyword);
        }
    }
}