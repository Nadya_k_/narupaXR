using UnityEngine;

namespace Narupa.Client.Utility.Extensions
{
    public static class MonoBehaviourExtensions
    {
        public static T GetOrCreateComponent<T>(this MonoBehaviour monoBehaviour) where T : UnityEngine.Component
        {
            return monoBehaviour.gameObject.GetOrCreateComponent<T>();
        }
    }
}