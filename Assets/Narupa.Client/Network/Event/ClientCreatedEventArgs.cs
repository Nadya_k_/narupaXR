// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

namespace Narupa.Client.Network.Event
{
    public class ClientCreatedEventArgs : ServerConnectionEventArgs
    {
        public ClientCreatedEventArgs(IServerConnection connection, NSB.Simbox.Client client) : base(connection)
        {
        }
    }
}