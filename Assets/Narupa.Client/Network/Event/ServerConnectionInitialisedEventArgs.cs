// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

namespace Narupa.Client.Network.Event
{
    public class ServerConnectionInitialisedEventArgs : ServerConnectionEventArgs
    {
        public ServerConnectionInitialisedEventArgs(IServerConnection connection) : base(connection)
        {
        }
    }
}