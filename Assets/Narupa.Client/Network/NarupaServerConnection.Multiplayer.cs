﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections.Generic;
using Nano.Transport.Variables;
using Narupa.VR.Multiplayer;

namespace Narupa.Client.Network
{
    public partial class NarupaServerConnection
    {
        private List<VRInteraction> VRInputs = new List<VRInteraction>();

        public void UpdateVRInputs(List<VRInteraction> vrInteractions)
        {
            VRInputs.Clear();
            foreach (var interaction in vrInteractions) VRInputs.Add(interaction);
        }

        public void UpdateVRPositions(List<VRInteraction> headsetPositions, List<VRInteraction> controllerPositions)
        {
            lock (VRPositions.Lock)
            {
                headsetPositions.Clear();
                controllerPositions.Clear();
                for (var i = 0; i < VRPositions.Count; i++)
                {
                    var interaction = VRPositions.Data[i];
                    if (interaction.Interaction.PlayerID == SteamVrObjectSender.PlayerId)
                        continue;
                    if ((VRDevice) interaction.DeviceType == VRDevice.Headset)
                        headsetPositions.Add(interaction);
                    else
                        controllerPositions.Add(interaction);
                }
            }
        }

        private void SendVrDevicePositions()
        {
            client.LocalVrDevicePositions.Count = VRInputs.Count;
            for (int i = 0; i < VRInputs.Count; ++i)
            {
                client.LocalVrDevicePositions.Data[i] = VRInputs[i];
            }

            client.OutgoingStreams.Flush(client.LocalVrDevicePositions.ID);
        }
    }
}