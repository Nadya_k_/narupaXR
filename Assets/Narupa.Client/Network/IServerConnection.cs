﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Nano;
using Nano.Client;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using NSB.Simbox.Debugging;

namespace Narupa.Client.Network
{
    /// <summary>
    /// General interface for a server connection.
    /// </summary>
    public interface IServerConnection
    {
        event Action ConnectionSucceeded;

        event Action ConnectionFailed;

        event Action ConnectionLost;

        bool Connected { get; }
    }
}