﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

namespace Narupa.Client.Network
{
	
	/// <summary>
	/// General interface that abstracts the ability to send messages to the server
	/// </summary>
	public interface IMessageSender 
	{

		/// <summary>
		/// Send a message with a certain address
		/// </summary>
		void SendMessage(string address, params object[] parameters);
		
	}
}
