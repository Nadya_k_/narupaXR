﻿using System;
using System.IO;
using Nano.Client;
using Newtonsoft.Json;
using UnityEngine;

namespace Narupa.Client.Network
{
    /// <summary>
    ///     Represents a JSON config file for Network settings, such as cloud connection
    /// </summary>
    internal class NetworkConfig
    {
        public bool AutoConnect = false;

        public string Descriptor;

        public bool Direct = false;

        public string SessionKey;

        public string Uri;

        public bool IsDirectConnection => !string.IsNullOrEmpty(Uri) && Direct;

        public bool HasCloudConnectionInfo => !string.IsNullOrEmpty(Descriptor) && !string.IsNullOrEmpty(SessionKey) &&
                                              !string.IsNullOrEmpty(Uri);

        public CloudConnectionInfo CloudConnectionInfo
        {
            get
            {
                return new CloudConnectionInfo(Descriptor, new Uri(Uri), SessionKey);
            } 
            set
            {
                Descriptor = value.Descriptor;
                Uri = value.ConnectionString.Uri.ToString();
                SessionKey = value.SessionKey;
            }
        }

        public static NetworkConfig Load(string serverPath)
        {
            if (serverPath == string.Empty)
                serverPath = $"{Application.dataPath}/ServerSettings/server.json";
            return LoadFromFile(serverPath);
        }

        public static NetworkConfig LoadFromFile(string path)
        {
            return File.Exists(path) ? LoadFromString(File.ReadAllText(path)) : null;
        }

        public static NetworkConfig LoadFromString(string json)
        {
            try
            {
                return JsonConvert.DeserializeObject<NetworkConfig>(json);
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                return null;
            }
        }

        public void SaveToFile(string path)
        {
            var directory = Path.GetDirectoryName(path);
            if (directory != null)
                Directory.CreateDirectory(directory);

            File.WriteAllText(path, SaveToString());
        }

        public string SaveToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}