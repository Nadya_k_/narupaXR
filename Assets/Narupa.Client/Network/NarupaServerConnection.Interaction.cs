﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections;
using System.Collections.Generic;
using Nano.Transport.Variables.Interaction;
using UnityEngine;


namespace Narupa.Client.Network
{
    public partial class NarupaServerConnection
    {
        public void ClearInputs()
        {
            this.Inputs.Clear();
        }
        
        /// <summary>
        ///     Called once per frame, copies interactionsToTransmit into the <see cref="NSBFrameReader" /> to be sent to the
        ///     server.
        /// </summary>
        public void CopyInteractions(List<Interaction> interactionsToTransmit, List<IList<int>> atomsToSelectTransmit)
        {
            Interactions.Clear();
            AtomsToSelect.Clear();
            var i = 0;
            foreach (var interaction in interactionsToTransmit)
            {
                Interactions.Add(interaction);
                //Copy over the selected atoms as well.
                AtomsToSelect.Add(atomsToSelectTransmit[i]);
                i++;
            }
        }
        
        private void SendInteractions()
        {
            client.Interactions.Count = Interactions.Count;
            int atomsSelected = 0;
            for (int i = 0; i < Interactions.Count; ++i)
            {
                client.Interactions.Data[i] = Interactions[i];
                if (client.InteractionAtoms == null)
                    continue;
                //Add all the selected atoms to the stream.
                foreach (int atomIndex in AtomsToSelect[i])
                {
                    client.InteractionAtoms.Data[atomsSelected] = atomIndex;
                    atomsSelected++;
                }
                //Add a -1 to the selected atoms list to denote the end of the list.
                client.InteractionAtoms.Data[atomsSelected] = -1;
                atomsSelected++;
            }
            if (client.InteractionAtoms != null)
            {
                client.InteractionAtoms.Count = atomsSelected;
                client.OutgoingStreams.Flush(client.InteractionAtoms.ID);
            }

            client.OutgoingStreams.Flush(client.Interactions.ID);
        }
    }
}