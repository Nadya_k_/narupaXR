﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using UnityEngine;

namespace Narupa.Client.UI.Pointer
{
    /// <summary>
    /// 	Simple implementation of a pointer, position an object with a Renderer at the intersection point and drawing a
    /// 	ray between the source and the target using a LineRenderer
    /// </summary>
    public class UiPointerRay : UiPointerSource
    {
        public Renderer RayTarget;

        public LineRenderer Ray;

        private void Start()
        {
            UpdatePointer(false, Vector3.zero);
        }

        public override void UpdatePointer(bool active, Vector3 rayTarget)
        {
            // Disable ray if it would mean the pointer is not aiming forward
            var forward = transform.forward;
            var rayDirection = (rayTarget - transform.position).normalized;
            var dot = Vector3.Dot(forward, rayDirection);
            if (dot < 0.95f)
                active = false;

            Ray.enabled = active;
            RayTarget.enabled = active;
            Ray.positionCount = 2;
            Ray.SetPositions(new[] {transform.position, rayTarget});
            RayTarget.transform.position = rayTarget;
        }
    }
}