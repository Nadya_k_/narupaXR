using System;

namespace Narupa.Client.Config
{
    [Serializable]
    public class IntParameter : NumericParameter<int>
    {
        public IntParameter(string name, int value, int min = 0, int max = 0) : base(name, value, min, max)
        {
        }
        
        public static implicit operator IntParameter(int d)
        {
            return new IntParameter(null, d);
        }

    }
}