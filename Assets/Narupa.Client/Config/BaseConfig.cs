using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Narupa.Client.Config
{
    /// <summary>
    /// Base class for some form of config file, containing parameters. The range of values for numeric parameters is
    /// specified in code, such that it is easy for both unity inspectors and in-app UI to alter the parameters.
    /// </summary>
    [Serializable]
    public abstract class BaseConfig
    {
        protected BaseConfig()
        {
            SetupParameters();
        }

        protected virtual void SetupParameters()
        {
        }

    }
}