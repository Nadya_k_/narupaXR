using System;

namespace Narupa.Client.Config
{
    [Serializable]
    public class FloatParameter : NumericParameter<float>
    {
        public FloatParameter(string name, float value, float min = 0, float max = 0) : base(name, value, min, max)
        {
        }

        public static implicit operator FloatParameter(float d)
        {
            return new FloatParameter(null, d);
        }
        
    }
}