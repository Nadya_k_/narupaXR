using System;

namespace Narupa.Client.Config
{
    [Serializable]
    public class BoolParameter : Parameter<bool>
    {
        public BoolParameter(string name, bool value) : base(name, value)
        {
        }
        
        public static implicit operator BoolParameter(bool d)
        {
            return new BoolParameter(null, d);
        }
    }
}