namespace Narupa.Client.Config
{
    internal interface ICopyable<in T>
    {
        void CopyFrom(T source);
    }
}