﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.IO;
using Narupa.Client.FileSystem;

namespace Narupa.Client.Network
{
	/// <summary>
	/// An IFileSystem implementation that uses the local computer
	/// </summary>
	public class LocalFileSystem : IFileSystem {

		public IEnumerable<string> GetDirectories(string path)
		{
			return Directory.EnumerateDirectories(path);
		}

		public IEnumerable<string> GetFiles(string path)
		{
			return Directory.EnumerateFiles(path);
		}
		
	}
}
