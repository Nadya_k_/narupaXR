﻿// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;

namespace Narupa.Client.FileSystem
{
	/// <summary>
	/// 	Represents a generic file system, with directories and files
	/// </summary>
	public interface IFileSystem
	{

		/// <summary>
		/// Get all subdirectories at a certain path
		/// </summary>
		IEnumerable<string> GetDirectories(string path);
		
		/// <summary>
		/// Get all files in a certain path
		/// </summary>
		IEnumerable<string> GetFiles(string path);

	}
}
