// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Linq;
using Nano.Client;
using Nano.Science;
using Narupa.Client.Render.Property;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using NSB.Simbox.Topology;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Narupa.Client.Render.Context
{
    /// <inheritdoc />
    /// <summary>
    ///     RenderContext that renders a Frame, treating it as a Vertex/Edge graph.
    /// </summary>
    public class FrameRenderContext : RenderContext
    {
        [RenderProperty("VERTEX.ELEMENT", WriteOnly = true)]
        private IndexedProperty<ushort> atomElement;

        [RenderProperty("VERTEX.INFO", WriteOnly = true)]
        private IndexedProperty<TopologyAtom> atomInfo;

        [RenderProperty("VERTEX.POSITION", WriteOnly = true)]
        private IndexedProperty<Vector3> atomPosition;

        [RenderProperty("EDGES", WriteOnly = true)]
        private IndexedProperty<BondPair> bonds;

        /// <summary>
        ///     The source of the Frame for this context
        /// </summary>
        public FrameSource FrameSource;

        [SerializeField] private int topologyUpdate = -1;

        /// <summary>
        ///     The current Frame that should be rendered
        /// </summary>
        protected NSBFrame Frame => FrameSource.Component.Frame;

        /// <summary>
        ///     Gets the raw data from the frame in a way
        /// </summary>
        /// <param name="frame"></param>
        private void ApplyFrame(NSBFrame frame)
        {
            atomPosition.Set(frame.AtomPositions);
            atomElement.Set(frame.AtomTypes);
            atomInfo.Set(frame.Topology.AtomsByAbsoluteIndex);
            bonds.Set(frame.BondPairs);
        }

        /// <inheritdoc />
        public override void Refresh()
        {
            if (Frame == null || Frame.AtomCount == 0)
                return;

            // Check if topology has been completely loaded
            if (Frame.Topology.AtomsByAbsoluteIndex.Count != Frame.AtomCount)
                return;

            ApplyFrame(Frame);

            // Check if the topology has updated
            if (Frame.TopologyUpdateCount != topologyUpdate)
            {
                topologyUpdate = Frame.TopologyUpdateCount;
                OnTopologyChanged();

                // Trigger the TopologyUpdated on any renderer which implements ITopologyUpdatedHandler, to inform
                // renderers that the topology has changed without requiring them to reference an actual 
                // FrameRenderContext
                foreach (var childRenderer in Renderers)
                    ExecuteEvents.Execute<ITopologyUpdatedHandler>(childRenderer.gameObject, null,
                        (x, y) => x.TopologyUpdated());
            }

            OnRefresh();
            base.Refresh();
        }

        /// <summary>
        ///     Called on each frame just before each child is rendered
        /// </summary>
        protected virtual void OnRefresh()
        {
        }

        /// <summary>
        ///     Called when the frame's topology is updated
        /// </summary>
        protected virtual void OnTopologyChanged()
        {
        }
    }
}