// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections;
using System.Collections.Generic;

namespace Narupa.Client.Render.Property
{
    public class IndexedProperty<T> : IIndexed<T>
    {
        private IIndexed<T> indexed;

        public IndexedProperty()
        {
            Set(new T[0]);
        }

        public IndexedProperty(IIndexed<T> indexed)
        {
            Set(indexed);
        }

        public bool HasValue => indexed != null;

        public T this[int i]
        {
            get { return indexed[i]; }
            set { indexed[i] = value; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return indexed.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public int Count => indexed.Count;

        public void Set(IIndexed<T> indexedNew)
        {
            indexed = indexedNew;
        }

        public void Set(IList<T> collection)
        {
            indexed = new IndexedCollection<T>(collection);
        }

        public void Resize(int count)
        {
            Set(new T[count]);
        }

        public void Clear()
        {
            indexed = null;
        }
    }
}