// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Narupa.Client.Render.Property
{
    /// <summary>
    ///     Attribute to mark fields of a RendererBehaviour to be shared between all renderers which belong to the
    ///     RenderContext
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class RenderProperty : Attribute
    {
        /// <summary>
        ///     Defines a render property with an identifier 'id'
        /// </summary>
        /// <param name="id"></param>
        public RenderProperty(string id)
        {
            Id = id;
        }

        /// <summary>
        ///     Identifying string used to index the various fields
        /// </summary>
        public string Id { get; }

        /// <summary>
        ///     Does this renderer only require write access. Not currently used
        /// </summary>
        public bool WriteOnly { get; set; }

        /// <summary>
        ///     Does this renderer only require read access. Not currently used
        /// </summary>
        public bool ReadOnly { get; set; }

        /// <summary>
        ///     Does this field need to be assigned for this renderer to work
        /// </summary>
        public bool Required { get; set; }
    }
}