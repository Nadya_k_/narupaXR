using System.Collections.Generic;
using Narupa.Client.Render.Context;
using Narupa.Client.Render.Property;
using Narupa.Client.Render.Renderer;
using NSB.Simbox.Topology;
using UnityEngine;

namespace Narupa.Client.Render.Calculator
{
    public class SecondaryStructureCalculator : RendererBehaviour, ITopologyUpdatedHandler
    {
        [RenderProperty("VERTEX.POSITION", ReadOnly = true)]
        private IndexedProperty<Vector3> atomPosition;
        
        [RenderProperty("VERTEX.INFO", ReadOnly = true)]
        private IndexedProperty<TopologyAtom> atomInfo;
        
        [RenderProperty("VERTEX.SS.TYPE", WriteOnly = true)]
        private IndexedProperty<char> atomSsType;
        
        public void TopologyUpdated()
        {
            CalculateResidues();
            CalculateHydrogenBonds();
        }

        public override void Refresh()
        {
            CalculateHydrogenBonds();
        }

        [SerializeField] private float cutoff;

        private void CalculateHydrogenBonds()
        {
            foreach (var residue1 in residueMap.Values)
            {
                residue1.BondedResidues.Clear();
                foreach (var residue2 in residueMap.Values)
                {
                    if (residue1.Id == residue2.Id)
                        continue;
                    var carbon = atomPosition[residue1.Carbon];
                    var nitrogen = atomPosition[residue1.Nitrogen];
                    var hydrogen = atomPosition[residue1.Hydrogen];
                    var oxygen = atomPosition[residue1.Oxygen];
                    var on = Vector3.Distance(oxygen, nitrogen);
                    var ch = Vector3.Distance(carbon, hydrogen);
                    var oh = Vector3.Distance(oxygen, hydrogen);
                    var cn = Vector3.Distance(carbon, nitrogen);
                    var val = 0.42 * 0.2f * 332f * (1 / on + 1 / ch - 1 / oh - 1 / cn);
                    if (val > cutoff)
                        continue;
                    residue1.BondedResidues.Add(residue2.Id);
                }
            }

            foreach (var residue in residueMap.Values)
            {
                residue.Type = '-';
            }

            foreach (var i in residueMap.Keys)
            {
                if (Turn(i, 4) && Turn(i - 1, 4))
                    for (var k = i; k < i + 4; k++)
                        residueMap[k].Type = 'H';

                if (Turn(i, 5) && Turn(i - 1, 5))
                    for (var k = i; k < i + 5; k++)
                        if (residueMap[k].Type != '-')
                            residueMap[k].Type = 'I';

                if (Turn(i, 3) && Turn(i - 1, 3))
                    for (var k = i; k < i + 3; k++)
                        if (residueMap[k].Type != '-')
                            residueMap[k].Type = 'G';
            }

            atomSsType.Resize(atomPosition.Count);
            for (var i = 0; i < atomPosition.Count; i++)
            {
                atomSsType[i] = residueMap[atomInfo[i].Parent.Id].Type;
            }
        }

        private bool Bonded(int i, int j)
        {
            return residueMap.ContainsKey(i) && residueMap[i].BondedResidues.Contains(j);
        }

        private bool Turn(int i, int size)
        {
            return Bonded(i, i + size);
        }

        private void CalculateResidues()
        {
            residueMap.Clear();
            // Calculate all residues
            foreach (var atom in atomInfo)
            {
                var residue = GetResidue(atom.Parent.Id);
                switch (atom.Name)
                {
                    case "CA":
                        residue.AlphaCarbon = atom.Index;
                        break;
                    case "C":
                        residue.Carbon = atom.Index;
                        break;
                    case "H":
                        residue.Hydrogen = atom.Index;
                        break;
                    case "N":
                        residue.Nitrogen = atom.Index;
                        break;
                    case "O":
                        residue.Oxygen = atom.Index;
                        break;
                }
            }
        }

        private Residue GetResidue(int id)
        {
            if (!residueMap.ContainsKey(id))
                residueMap[id] = new Residue() {Id = id};
            return residueMap[id];
        }

        private Dictionary<int, Residue> residueMap = new Dictionary<int, Residue>();

        class Residue
        {
            public int Id;
            public int Carbon;
            public int Nitrogen;
            public int Hydrogen;
            public int Oxygen;
            public int AlphaCarbon;
            public List<int> BondedResidues = new List<int>();
            public char Type;
        }
    }
}