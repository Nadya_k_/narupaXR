// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science;
using Narupa.Client.Render.Property;
using Narupa.Client.Render.Renderer;
using UnityEngine;

namespace Narupa.Client.Render.Radius
{
    /// <inheritdoc />
    /// <summary>
    ///     General renderer for calculating the radius of a vertex based on the Element
    /// </summary>
    public abstract class ElementRadiusRenderer : RadiusRenderer
    {
        [RenderProperty("VERTEX.ELEMENT")] private IndexedProperty<ushort> atomElement;

        [RenderProperty("VERTEX.POSITION")] private IndexedProperty<Vector3> atomPosition;

        /// <inheritdoc />
        protected override void RefreshRadii(IndexedCollection<float> target)
        {
            target.Resize(atomPosition.Count);
            for (var i = 0; i < atomPosition.Count; i++)
            {
                var element = atomElement[i];
                target[i] = GetElementRadius(element);
            }
        }

        /// <summary>
        ///     Get the radii associated with the element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected abstract float GetElementRadius(ushort element);
    }
}