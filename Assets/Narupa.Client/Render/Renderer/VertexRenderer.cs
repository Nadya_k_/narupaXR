// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.Client.Render.Property;
using UnityEngine;

namespace Narupa.Client.Render.Renderer
{
    public class VertexRenderer : RendererBehaviour
    {
        [RenderProperty("VERTEX.COLOR", ReadOnly = true)]
        protected IndexedProperty<Color> atomColor;

        [RenderProperty("VERTEX.PALETTE.INDEX", ReadOnly = true)]
        protected IndexedProperty<byte> atomPaletteIndex;

        [RenderProperty("VERTEX.POSITION", ReadOnly = true, Required = true)]
        protected IndexedProperty<Vector3> atomPosition;

        [RenderProperty("VERTEX.RADIUS", ReadOnly = true, Required = true)]
        protected IndexedProperty<float> atomRadius;
    }
}