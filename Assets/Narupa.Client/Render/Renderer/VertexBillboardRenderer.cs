// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.Client.Config;
using Narupa.Client.Render.Utility;
using NSB.MMD.MeshGeneration;
using NSB.MMD.RenderingTypes;
using UnityEngine;

namespace Narupa.Client.Render.Renderer
{
    public class VertexBillboardRenderer : VertexRenderer
    {
        private readonly ParticleBatchSet batches = new ParticleBatchSet();

        public bool ForceParticles;

        private MeshTool mesh;
        private readonly ParticleBatchRenderer particlesRenderer = new ParticleBatchRenderer();

        [SerializeField] [Header("Setup")] [Tooltip("Texture to use as the billboarded sprite")]
        private Texture2D ParticleTexture;

        private Material pointsMaterial;
        private MeshFilter pointsMeshFilter;

        private MeshRenderer pointsMeshRenderer;

        [Header("Shaders")] [SerializeField] [Tooltip("Shader for rendering a mesh of colored points as points")]
        private Shader pointsShader;

        private Material pointsToBillboardsMaterial;

        [SerializeField]
        [Tooltip("Shader for rendering a mesh of colored points as billboarded sprites (using a geometry shader)")]
        private Shader pointsToBillboardsShader;

        private Material quadsMaterial;

        [SerializeField] [Tooltip("Shader for rendering billboarded sprites via a Unity particle system")]
        private Shader quadsShader;

        //[Section]
        public PointsConfig Settings = new PointsConfig();

        private void Awake()
        {
            pointsMaterial = new Material(pointsShader);

            pointsToBillboardsMaterial = new Material(pointsToBillboardsShader);
            pointsToBillboardsMaterial.SetTexture("_MainTex", ParticleTexture);

            quadsMaterial = new Material(quadsShader);
            quadsMaterial.SetTexture("_MainTex", ParticleTexture);

            // points mesh
            AddMesh(ref pointsMeshFilter, ref pointsMeshRenderer);

            mesh = new MeshTool(new Mesh(), MeshTopology.Points);
            pointsMeshFilter.sharedMesh = mesh.mesh;

            // fallback particle system
            var quad = new Mesh();
            MeshGeneration.GenerateAtomQuad(quad, 1);

            particlesRenderer.Initialise(transform);
            particlesRenderer.SetMesh(quad, true);
            particlesRenderer.SetMaterial(quadsMaterial);

            batches.SetBatchSize(quad);
        }

        public override void Refresh()
        {
            var useMesh = !ForceParticles;

            if (useMesh && Settings.Scaling.Value > 0 && pointsToBillboardsMaterial.shader.isSupported)
            {
                pointsMeshRenderer.sharedMaterial = pointsToBillboardsMaterial;
                // TODO: use model scaling in the geometry shader...
                pointsToBillboardsMaterial.SetFloat("_Scaling", Settings.Scaling.Value * transform.lossyScale.x);
            }
            else if (Settings.Scaling.Value > 0)
            {
                useMesh = false;
                quadsMaterial.SetFloat("_Scaling", Settings.Scaling.Value);
            }
            else
            {
                useMesh = true;
                pointsMeshRenderer.sharedMaterial = pointsMaterial;
            }

            if (useMesh)
            {
                particlesRenderer.Clear();
                pointsMeshRenderer.enabled = true;

                MeshUtility.GenerateAtomPoints(mesh, atomPosition, atomColor);

                // vertex per selected atom
                for (var i = 0; i < atomPosition.Count; ++i)
                {
                    var data = default(Vector4);
                    data.x = atomRadius[i];

                    mesh.uv0s[i] = data;
                }

                mesh.Apply(uv0s: true);
            }
            else
            {
                pointsMeshRenderer.enabled = false;

                batches.GenerateParticles(this, atomPosition.Count, GenerateParticle);
                particlesRenderer.RenderBatchSet(batches);
            }
        }

        private static void GenerateParticle(VertexBillboardRenderer data,
            int id,
            ref ParticleSystem.Particle particle)
        {
            particle.position = data.atomPosition[id];
            particle.startColor = data.Settings.UseOverrideColor ? (Color) data.Settings.OverrideColor : data.atomColor[id];
            particle.startSize = data.atomRadius[id] * data.Settings.Scaling.Value;
        }

        [Serializable]
        public class PointsConfig : BaseConfig
        {
            public FloatParameter Scaling = 1f;
            public BoolParameter UseOverrideColor = false;
            public ColorParameter OverrideColor = Color.black;

            protected override void SetupParameters()
            {
                Scaling.SetRange(0, 1);
            }
        }
    }
}