// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Client;
using Narupa.Client.Render.Property;
using UnityEngine;

namespace Narupa.Client.Render.Renderer
{
    /// <inheritdoc />
    /// <summary>
    ///     General renderer for drawing edges
    /// </summary>
    public class EdgeRenderer : RendererBehaviour
    {
        [RenderProperty("VERTEX.COLOR", ReadOnly = true)]
        protected IndexedProperty<Color> atomColor;

        [RenderProperty("VERTEX.PALETTE.INDEX", ReadOnly = true)]
        protected IndexedProperty<byte> atomPaletteIndex;

        [RenderProperty("VERTEX.POSITION", ReadOnly = true)]
        protected IndexedProperty<Vector3> atomPosition;

        [RenderProperty("EDGE.COLOR", ReadOnly = true)]
        protected IndexedProperty<Color> bondColor;

        [RenderProperty("EDGES", ReadOnly = true, Required = true)]
        protected IndexedProperty<BondPair> bonds;

        [RenderProperty("PALETTE", ReadOnly = true)]
        protected ValueProperty<Texture2D> palette;

        protected Color GetEdgeColor(int index, BondPair edge)
        {
            if (atomPaletteIndex.Count > 0)
            {
                var elementA = atomPaletteIndex[edge.A];
                var elementB = atomPaletteIndex[edge.B];
                return new Color32(elementA, elementB, 255, 255);
            }

            if (bondColor.Count > 0)
                return bondColor[index];
            return Color32.LerpUnclamped(atomColor[edge.A], atomColor[edge.B], 0.5f);
        }
    }
}