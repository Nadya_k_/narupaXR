// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Narupa.Client.Utility.Extensions;
using UnityEngine;
using UnityEngine.Rendering;

namespace Narupa.Client.Render.Renderer
{
    /// <inheritdoc />
    /// <summary>
    ///     Base class for a renderer. The OnContextChanged method should be used to get properties.
    /// </summary>
    public abstract class RendererBehaviour : MonoBehaviour
    {
        /// <summary>
        ///     Called each frame to refresh the renderer
        /// </summary>
        public virtual void Refresh()
        {
        }

        /// <summary>
        ///     Convenience method to find or create MeshFilter and MeshRenderer components
        ///     for this renderer, and configure them to ignore lighting and shadows.
        /// </summary>
        protected void AddMesh(ref MeshFilter filter, ref MeshRenderer renderer)
        {
            filter = gameObject.GetOrCreateComponent<MeshFilter>();
            renderer = gameObject.GetOrCreateComponent<MeshRenderer>();
            
            renderer.receiveShadows = false;
            renderer.shadowCastingMode = ShadowCastingMode.Off;
            renderer.reflectionProbeUsage = ReflectionProbeUsage.Off;
            renderer.lightProbeUsage = LightProbeUsage.Off;
        }
    }
}