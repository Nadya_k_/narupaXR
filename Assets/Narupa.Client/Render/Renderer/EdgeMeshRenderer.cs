// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Narupa.Client.Config;
using NSB.MMD.MeshGeneration;
using NSB.MMD.RenderingTypes;
using NSB.Utility;
using UnityEngine;

namespace Narupa.Client.Render.Renderer
{
    /// <inheritdoc />
    /// <summary>
    ///     Renders edges
    /// </summary>
    public class EdgeMeshRenderer : EdgeRenderer
    {
        private readonly ParticleBatchSet particleBatches = new ParticleBatchSet();
        private readonly ParticleBatchRenderer particleRenderer = new ParticleBatchRenderer();

        [SerializeField] private Material bondsMaterial;

        [Header("Shaders")] [SerializeField] private Shader bondsShader;

        private bool meshDirty = true;

        [Header("Setup")] [SerializeField] private Config settings = new Config();

        private Config Settings => settings;

        private void Awake()
        {
            bondsMaterial = new Material(bondsShader);
            Settings.PrismSides.ValueChanged += (sender, args) => meshDirty = true;
            Settings.EndCaps.ValueChanged += (sender, args) => meshDirty = true;
        }

        public override void Refresh()
        {
            base.Refresh();

            if (bonds.Count == 0)
            {
                particleRenderer.Clear();
                return;
            }

            if (meshDirty) RegenerateMesh();

            if (atomPaletteIndex.Count > 0)
            {
                bondsMaterial.EnableKeyword("GRADIENT");
                bondsMaterial.SetTexture("_Palette", palette);
            }
            else
            {
                bondsMaterial.DisableKeyword("GRADIENT");
            }

            bondsMaterial.SetFloat("_Sharpness", Settings.BlendSharpness.Value);

            particleBatches.GenerateParticles(this, bonds.Count, UpdateParticle);
            particleRenderer.RenderBatchSet(particleBatches);
        }

        private static void UpdateParticle(EdgeMeshRenderer data,
            int index,
            ref ParticleSystem.Particle particle)
        {
            var edge = data.bonds[index];
            var posA = data.atomPosition[edge.A];
            var posB = data.atomPosition[edge.B];

            var color = data.GetEdgeColor(index, edge);

            particle.startColor = color;

            var dir = FasterMath.Sub(posB, posA);
            var rot = Quaternion.LookRotation(dir).eulerAngles;

            particle.position = FasterMath.Lerp(posA, posB, 0.5f);
            particle.rotation3D = rot;

            Vector3 scale;
            scale.y = data.Settings.Radius.Value;
            scale.z = dir.magnitude;
            scale.x = data.Settings.Radius.Value;

            particle.startSize3D = scale;
        }

        private void RegenerateMesh()
        {
            meshDirty = false;
            var mesh = new Mesh();
            MeshGeneration.GenerateBondPrism(mesh, settings.PrismSides, capped: settings.EndCaps);
            particleRenderer.Initialise(transform);
            particleRenderer.SetMesh(mesh);
            particleRenderer.SetMaterial(bondsMaterial);
            particleBatches.SetBatchSize(ParticleBatchRenderer.VertexLimit / mesh.vertexCount);
        }

        [Serializable]
        public class Config : BaseConfig
        {
            public FloatParameter BlendSharpness = 0.5f;
            public IntParameter Darken = 64;
            public BoolParameter EndCaps = false;
            public IntParameter PrismSides = 3;
            public FloatParameter Radius = 0.04f;

            protected override void SetupParameters()
            {
                Darken.SetRange(0, 255);
                PrismSides.SetRange(3, 16);
                Radius.SetRange(0, 0.1f);
                BlendSharpness.SetRange(0, 1);
            }
        }
    }
}