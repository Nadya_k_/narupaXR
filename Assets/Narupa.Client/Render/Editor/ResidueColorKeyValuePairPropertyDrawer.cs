﻿using Nano.Science;
using Narupa.Client.Render.Definition;
using UnityEditor;
using UnityEngine;

namespace Narupa.Client.Render.Editor
{
	[CustomPropertyDrawer(typeof(ResidueColorDictionary.ResidueColorKeyValuePair))]
	public class ResidueColorKeyValuePairPropertyDrawer : PropertyDrawer {
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var w = 96;
			var colorRect = new Rect(position.xMax-w, position.y, w, position.height);
			var numberRect = new Rect(position.x, position.y, position.width-8-w, position.height);
			EditorGUI.PropertyField(numberRect, property.FindPropertyRelative("Regex"), GUIContent.none);
			EditorGUI.PropertyField(colorRect, property.FindPropertyRelative("Color"), GUIContent.none);
		}
	}
}
