// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science;
using Narupa.Client.Render.Property;
using Narupa.Client.Render.Renderer;
using UnityEngine;

namespace Narupa.Client.Render.Colour
{
    /// <inheritdoc />
    /// <summary>
    ///     General renderer that colors atoms according to the Element
    /// </summary>
    public abstract class ElementColorRenderer : ColorRenderer
    {
        [RenderProperty("VERTEX.ELEMENT", ReadOnly = true, Required = true)]
        private IndexedProperty<ushort> atomElement;

        /// <inheritdoc />
        protected override void RefreshColors(IndexedCollection<Color> target)
        {
            target.Resize(atomElement.Count);
            for (var i = 0; i < atomElement.Count; i++)
                target[i] = GetElementColor(atomElement[i]);
        }

        /// <summary>
        ///     Get the color based on the atom element
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected abstract Color GetElementColor(ushort element);
    }
}