// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Narupa.Client.Render.Definition;
using NSB.Simbox.Topology;
using UnityEngine;
using UnityEngine.Experimental.UIElements;

namespace Narupa.Client.Render.Colour
{
    /// <inheritdoc />
    /// <summary>
    ///     Colors the atoms according to the residue name
    /// </summary>
    public class ResidueColorRenderer : TopologyInfoRenderer
    {
        [SerializeField] private ResidueColorDictionary dictionary;

        public List<Tuple<Regex, Color>> matches = new List<Tuple<Regex, Color>>();

        private void Awake()
        {
            foreach (var option in dictionary.Dictionary)
            {
                var match = new Regex(option.Key);
                matches.Add(new Tuple<Regex, Color>(match, option.Value));
            }
        }

        /// <inheritdoc />
        protected override Color GetAtomColor(TopologyAtom element)
        {
            var resId = element.Parent.Name.Split('-')[0];
            foreach(var match in matches)
                if (match.Item1.IsMatch(resId))
                    return match.Item2;
            return GetDefaultColor();
        }

        /// <inheritdoc />
        protected override Color GetDefaultColor()
        {
            return dictionary.DefaultColor;
        }
    }
}