// Copyright (c) Alex Jamieson-Binnie. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Client;
using Narupa.Client.Render.Property;
using NSB.MMD.MeshGeneration;
using UnityEngine;

namespace Narupa.Client.Render.Utility
{
    public static class MeshUtility
    {
        public static void GenerateBondLines(MeshTool mesh, IIndexed<BondPair> bonds, IIndexed<Vector3> atoms,
            IIndexed<Color> atomColors)
        {
            mesh.SetVertexCount(atoms.Count);
            mesh.SetIndexCount(bonds.Count * 2);

            // vertex per selected atom
            for (var i = 0; i < atoms.Count; ++i)
            {
                mesh.positions[i] = atoms[i];
                mesh.colors[i] = atomColors[i];
            }

            // line between each pair of bonded vertices
            for (var i = 0; i < bonds.Count; ++i)
            {
                var bond = bonds[i];

                mesh.indices[i * 2 + 0] = bond.A;
                mesh.indices[i * 2 + 1] = bond.B;
            }

            mesh.mesh.Clear();
            mesh.Apply(true,
                colors: true,
                indices: true);
        }

        public static void GenerateBondLines(MeshTool mesh, IIndexed<BondPair> bonds, IIndexed<Color> bondColors,
            IIndexed<Vector3> atoms)
        {
            mesh.SetVertexCount(bonds.Count * 2);
            mesh.SetIndexCount(bonds.Count * 2);

            // line between each pair of bonded vertices
            for (var i = 0; i < bonds.Count; ++i)
            {
                var bond = bonds[i];
                mesh.positions[i * 2 + 0] = atoms[bond.A];
                mesh.positions[i * 2 + 1] = atoms[bond.B];
                mesh.colors[i * 2 + 0] = bondColors[i];
                mesh.colors[i * 2 + 1] = bondColors[i];
                mesh.indices[i * 2 + 0] = i * 2 + 0;
                mesh.indices[i * 2 + 1] = i * 2 + 1;
            }

            mesh.mesh.Clear();
            mesh.Apply(true,
                colors: true,
                indices: true);
        }

        public static void GenerateAtomPoints(MeshTool mesh, IIndexed<Vector3> atomPositions,
            IIndexed<Color> atomColors)
        {
            mesh.mesh.Clear();

            mesh.SetVertexCount(atomPositions.Count);
            mesh.SetIndexCount(atomPositions.Count);

            // vertex per selected atom
            for (var i = 0; i < atomPositions.Count; ++i)
            {
                mesh.positions[i] = atomPositions[i];
                mesh.colors[i] = atomColors[i];
                mesh.indices[i] = i;
            }

            mesh.Apply(true,
                colors: true,
                indices: true);
        }
    }
}