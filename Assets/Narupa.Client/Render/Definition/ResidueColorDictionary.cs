using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Narupa.Client.Render.Definition
{
    /// <inheritdoc />
    /// <summary>
    ///     Stores key-value pair of atomic number and color, for defining CPK style coloring
    /// </summary>
    [CreateAssetMenu(menuName = "Definition/Residue-Color Dictionary")]
    public class ResidueColorDictionary : ScriptableObject
    {
        [Serializable]
        public class ResidueColorKeyValuePair
        {
            public string Regex;
            public Color Color;
        }

        [SerializeField] private List<ResidueColorKeyValuePair> dictionary;
        [SerializeField] private Color defaultColor;
        public Dictionary<string, Color> Dictionary => dictionary.ToDictionary(t => t.Regex, t => t.Color);
        public Color DefaultColor => defaultColor;
    }
}