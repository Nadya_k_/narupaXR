﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.MMD.Base;
using UnityEngine;

namespace NSB.MMD
{
    public class NSBRepresentation : MMDRenderer
    {
        public enum Mode
        {
            Skeletal,
            BallAndStick,
            SpaceFilling,
        }

        [System.Serializable]
        public class NSBConfig : RendererConfig
        {
            [Header("Mode Switching")]
            public IntParameter Mode;
            public FloatParameter ModeSwitchTime;

            [Header("Selection Darkening")]
            public FloatParameter DarkenTime;
            public FloatParameter Darken;

            [Header("Atoms")]
            public IntParameter AtomMeshComplexity;
            public FloatParameter AtomHemisphereCull;

            [Header("Bonds")]
            public IntParameter BondPrismSides;
            public FloatParameter BondRadius;     
            public FloatParameter BondBlendSharpness;

            protected override void Setup()
            {
                Mode = AddInt("Mode", 1, 0, 2, false);
                ModeSwitchTime = AddFloat("Mode Switch Time", 0.2f, 0, .5f, dirtying: false);
                Darken = AddFloat("Darken", .25f, 0, 1, dirtying: false);
                DarkenTime = AddFloat("Darken Time", 0.4f, 0, 1f, dirtying: false);
                BondBlendSharpness = AddFloat("Bond Blending", 0f, 0, 1, dirtying: false);

                AtomMeshComplexity = AddInt(nameof(AtomMeshComplexity), 6, 0, 12);
                AtomHemisphereCull = AddFloat(nameof(AtomHemisphereCull), 0, 0, 1);

                BondPrismSides = AddInt(nameof(BondPrismSides), 3, 3, 16);
                BondRadius = AddFloat(nameof(BondRadius), 0.04f, 0, 0.1f, dirtying: false);
            }
        }

        [Header("Setup")]
        [SerializeField]
        private VDWRepresentation atoms;

        [SerializeField]
        private BondsRepresentation bonds;

        public NSBConfig Settings = new NSBConfig();

        public override RendererConfig Config => Settings;

        private float darken;
        private float darkenVelocity;

        private float area;
        private float areaVelocity;

        public override void Refresh()
        {
            float darkenTarget = Style.HighlightedAtoms != null && Style.HighlightedAtoms.Count > 0
                ? Settings.Darken.Value * 255
                : 255;
            darken = Mathf.SmoothDamp(darken, darkenTarget, ref darkenVelocity, Settings.DarkenTime.Value);
            atoms.Settings.Darken.Value = (int)Mathf.Min(darken, 255);
            bonds.Settings.Darken.Value = (int)Mathf.Min(darken, 255);

            float scaleTarget = 1;

            if (Settings.Mode.Value == (int)Mode.Skeletal) scaleTarget = 0f;
            else if (Settings.Mode.Value == (int)Mode.BallAndStick) scaleTarget = 1 / 3f;
            else if (Settings.Mode.Value == (int)Mode.SpaceFilling) scaleTarget = 1f;

            float areaTarget = Mathf.PI * scaleTarget * scaleTarget;
            area = Mathf.SmoothDamp(area, areaTarget, ref areaVelocity, Settings.ModeSwitchTime.Value);
            float scale = Mathf.Sqrt(area / Mathf.PI);

            atoms.Frame = Frame;
            atoms.Selection = Selection;
            atoms.Style = Style;

            atoms.Settings.Hemispheres.Value = true;
            atoms.Settings.Spin.Value = true;

            atoms.Settings.MeshComplexity.Value = Settings.AtomMeshComplexity.Value;
            atoms.Settings.Scaling.Value = scale;

            bonds.Frame = Frame;
            bonds.Selection = Selection;
            bonds.Style = Style;

            bonds.Settings.BlendSharpness.Value = Settings.BondBlendSharpness.Value;
            bonds.Settings.EndCaps.Value = area < 0.2f;
            bonds.Settings.PrismSides.Value = Settings.BondPrismSides.Value;
            bonds.Settings.Radius.Value = Settings.BondRadius.Value;

            bool showAtoms = area > 0;
            bool showBonds = area < .7f;

            atoms.gameObject.SetActive(showAtoms);
            bonds.gameObject.SetActive(showBonds);

            if (showAtoms) atoms.Refresh();
            if (showBonds) bonds.Refresh();
        }

        public override float GetAtomBoundingRadius(int id)
        {
            return Mathf.Max(atoms.GetAtomBoundingRadius(id),
                bonds.GetAtomBoundingRadius(id));
        }
    }
}