﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using NSB.MMD.Base;
using UnityEditor;
using UnityEngine;

namespace NSB.MMD.Editor
{
    public abstract class ParameterDrawer<T> : PropertyDrawer
        where T : IEquatable<T>
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            label = EditorGUI.BeginProperty(position, label, property);
            var param = EditorHelper.GetTargetObjectOfProperty(property) as RendererConfig.Parameter<T>;

            var value = property.FindPropertyRelative("_value");
            T prev = param._value;

            EditorGUI.indentLevel = 0;

            //position = EditorGUI.PrefixLabel(position, )

            if (typeof(T) == typeof(RendererConfig.ConfigColor))
            {
                var p = prev as RendererConfig.ConfigColor;
                var color = EditorGUI.ColorField(position, new GUIContent(property.displayName), p.color, false, false, false);

                if (color != p.color)
                {
                    param._value = (T) ((object) new RendererConfig.ConfigColor { color = color });

                    Undo.RecordObject(value.serializedObject.targetObject, property.displayName);
                }
            }
            else if (!param.Constrained)
            {
                EditorGUI.PropertyField(position, value, new GUIContent(property.displayName));
            }
            else if (typeof(T) == typeof(int))
            {
                EditorGUI.IntSlider(position, value, (int) (object) param.Min, (int) (object) param.Max, new GUIContent(property.displayName));
            }
            else if (typeof(T) == typeof(float))
            {
                EditorGUI.Slider(position, value, (float) (object) param.Min, (float) (object) param.Max, new GUIContent(property.displayName));
            }
        
            EditorGUI.EndProperty();

            property.serializedObject.ApplyModifiedProperties();

            if (!param._value.Equals(prev))
            {
                param.Config.SetChanged();
            }

        }
    }

    [CustomPropertyDrawer(typeof(RendererConfig.IntParameter))]
    public class IntParameterDrawer : ParameterDrawer<int> { }

    [CustomPropertyDrawer(typeof(RendererConfig.FloatParameter))]
    public class FloatParameterDrawer : ParameterDrawer<float> { }

    [CustomPropertyDrawer(typeof(RendererConfig.BoolParameter))]
    public class BoolParameterDrawer : ParameterDrawer<bool> { }

    [CustomPropertyDrawer(typeof(RendererConfig.ColorParameter))]
    public class ColorParameterDrawer : ParameterDrawer<RendererConfig.ConfigColor> { }
}