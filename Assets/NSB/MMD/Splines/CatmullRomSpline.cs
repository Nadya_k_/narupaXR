﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using System.Threading.Tasks;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Profiling;

namespace NSB.MMD.Splines
{
    public static class CatmulRomSpline 
    {
        public static int GetPointCount(int controlCount, int intermediates)
        {
            return controlCount + (controlCount - 1) * intermediates;
        }

        /// <summary>
        /// Using the given control points, write interpolated points to the output
        /// list following a Centripetal Catmull-Rom Spline. Intermediates is the
        /// number of points in each curve in addition to the control points.
        /// No idea what the alpha parameter means. The two ends are linearly
        /// interpolated absent enough control point data.
        /// </summary>
        public static void ComputeLine(List<Vector3> controls, 
            List<Vector3> output,
            int intermediates,
            float alpha = 0.5f,
            int spacing = 0,
            int offset = 0)
        {
            Profiler.BeginSample("CatmulRomSpline.ComputeLine");

            int count = GetPointCount(controls.Count, intermediates);
            int limit = (count - 1) * (1 + spacing) + offset;
            // TODO: manually resize? don't want to mess up other data already in output tho
            Assert.IsTrue(output.Count >= limit, $"Output list is too small ({output.Count}) to ComputeLine ({limit})!");

            int stride = intermediates + 1;
            float inv = 1f / stride;

            // starting curve (we're missing a control point before this curve)
            {
                Vector3 p1 = controls[0];
                Vector3 p2 = controls[1];

                Parallel.For(0, stride, v => output[v * (1 + spacing) + offset] = FasterMath.Lerp(p1, p2, v * inv));
            }
        
            // ending curve (we're missing a control point after this curve)
            {
                Vector3 p1 = controls[controls.Count - 2];
                Vector3 p2 = controls[controls.Count - 1];

                Parallel.For(0, stride + 1, v => output[(count - 1 - v) * (1 + spacing) + offset] = FasterMath.Lerp(p2, p1, v * inv));
            }


            // all other curves
            Parallel.For(1, controls.Count - 2, i =>
            {
                Vector3 p0 = controls[i - 1];
                Vector3 p1 = controls[i + 0];
                Vector3 p2 = controls[i + 1];
                Vector3 p3 = controls[i + 2];

                // first point in each curve is a control point (the last too, but 
                // it's the first point in the next curve)
                output[i * (stride + spacing) + offset] = p1;

                float t0 = 0.0f;
                float t1 = GetT(t0, p0, p1, alpha);
                float t2 = GetT(t1, p1, p2, alpha);
                float t3 = GetT(t2, p2, p3, alpha);

                for (int v = 1; v < stride; ++v)
                {
                    float u = v * inv;
                    float t = t1 * (1 - u) + t2 * u;

                    Vector3 A1 = FasterMath.Add(FasterMath.Mul(p0, (t1 - t) / (t1 - t0)), 
                        FasterMath.Mul(p1, (t - t0) / (t1 - t0)));
                    Vector3 A2 = FasterMath.Add(FasterMath.Mul(p1, (t2 - t) / (t2 - t1)),
                        FasterMath.Mul(p2, (t - t1) / (t2 - t1)));
                    Vector3 A3 = FasterMath.Add(FasterMath.Mul(p2, (t3 - t) / (t3 - t2)),
                        FasterMath.Mul(p3, (t - t2) / (t3 - t2)));

                    Vector3 B1 = FasterMath.Add(FasterMath.Mul(A1, (t2 - t) / (t2 - t0)),
                        FasterMath.Mul(A2, (t - t0) / (t2 - t0)));
                    Vector3 B2 = FasterMath.Add(FasterMath.Mul(A2, (t3 - t) / (t3 - t1)),
                        FasterMath.Mul(A3, (t - t1) / (t3 - t1)));

                    Vector3 C = FasterMath.Add(FasterMath.Mul(B1, (t2 - t) / (t2 - t1)),
                        FasterMath.Mul(B2, (t - t1) / (t2 - t1)));

                    output[i * (stride + spacing) + v + offset] = C;
                }
            });

            Profiler.EndSample();
        }

        private static float GetT(float t, Vector2 p0, Vector2 p1, float alpha)
        {
            float a = Mathf.Pow((p1.x - p0.x), 2.0f) + Mathf.Pow((p1.y - p0.y), 2.0f);
            float b = Mathf.Pow(a, 0.5f);
            float c = Mathf.Pow(b, alpha);

            return (c + t);
        }
    }
}
