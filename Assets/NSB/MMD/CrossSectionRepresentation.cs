﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Transport.Variables;
using NSB.MMD.Base;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;
using BoundingBox = SlimMath.BoundingBox;

namespace NSB.MMD
{
    public class CrossSectionRepresentation : MMDRenderer, ISelectionSource
    {
        [Serializable]
        public class CrossSectionConfig : RendererConfig
        {
            public FloatParameter Level;
            public IntParameter Resolution;

            protected override void Setup()
            {
                Level = AddFloat("Level", .5f, 0, 1f, dirtying: false);
                Resolution = AddInt("Resolution", 64);
            }
        }

        [Header("Shaders")]
        [SerializeField]
        private Shader quadShader;

        public CrossSectionConfig Settings = new CrossSectionConfig();
        public override RendererConfig Config => Settings;

        [Header("Debug")]
        [Range(0, 1)]
        public float minLevel;
        [Range(0, 1)]
        public float maxLevel;
        [Range(0, 1)]
        public float speed;

        private Texture2D sliceTexture;
        private Transform sliceTransform;
        private Material sliceMaterial;
        private byte[] sampleField;

        private NSBSelection selection { get; } = new NSBSelection();
        NSBSelection ISelectionSource.Selection => selection;

        private void Awake()
        {
            sliceMaterial = new Material(quadShader);

            var slice = new GameObject("Slice");
            var filter = slice.AddComponent<MeshFilter>();
            var renderer = slice.AddComponent<MeshRenderer>();

            var mesh = new Mesh();
            MeshGeneration.MeshGeneration.GenerateAtomQuad(mesh);

            sliceTransform = slice.transform;
            sliceTransform.SetParent(transform);
            slice.layer = sliceTransform.parent.gameObject.layer;

            renderer.sharedMaterial = sliceMaterial;
            filter.mesh = mesh;
        }

        public override void Refresh()
        {
            if (Settings.ResetDirty()) Regenerate();

            if (!CheckValidity()) return;

            Style.UsePalette(sliceMaterial);

            Settings.Level.Value = Mathf.Lerp(minLevel, maxLevel, Mathf.PingPong(Time.timeSinceLevelLoad * speed, 1));

            var bounds = Frame.GetVariable<BoundingBox>(VariableName.SimBoundingBox);

            float level = Mathf.Lerp(bounds.Minimum.Z, bounds.Maximum.Z, Settings.Level.Value);
            float xwidth = bounds.Maximum.X - bounds.Minimum.X;
            float ywidth = bounds.Maximum.Y - bounds.Minimum.Y;
            float zwidth = bounds.Maximum.Z - bounds.Minimum.Z;

            float ix = 1f / xwidth;
            float iy = 1f / ywidth;
            float iz = 1f / zwidth;

            int size = Settings.Resolution.Value;
            float inv = 1f / size;

            float xsize = xwidth * inv;
            float ysize = ywidth * inv;
            float zsize = zwidth * inv;

            var min = bounds.Minimum;

            int z = Mathf.FloorToInt(Settings.Level.Value * size);

            sliceTransform.localScale = new Vector3(xwidth, ywidth, zwidth);
            sliceTransform.localPosition = new Vector3(0, 0, min.Z + z * zsize);

            selection.Clear();
            selection.SetSourceAtomCount(Frame.AtomCount);
            for (int i = 0; i < Frame.AtomCount; ++i)
            {
                if (Frame.AtomPositions[i].z < level)
                {
                    selection.Add(i);
                }
            }

            for (int i = 0; i < sampleField.Length; ++i)
            {
                sampleField[i] = 0;
            }

            for (int i = 0; i < Frame.AtomCount; ++i)
            {
                Vector3 position = Frame.AtomPositions[i];
                float radius = Style.AtomRadiuses[i];

                position.x -= bounds.Minimum.X;
                position.y -= bounds.Minimum.Y;
                position.z -= bounds.Minimum.Z;

                Vector3 rpos = position;

                position.x *= ix;
                position.y *= iy;
                position.z *= iz;
            
                int min_x = Mathf.FloorToInt(Mathf.Clamp01(position.x - radius * ix) * size);
                int min_y = Mathf.FloorToInt(Mathf.Clamp01(position.y - radius * iy) * size);
                int min_z = Mathf.FloorToInt(Mathf.Clamp01(position.z - radius * iz) * size);

                int max_x = Mathf.FloorToInt(Mathf.Clamp01(position.x + radius * ix) * size);
                int max_y = Mathf.FloorToInt(Mathf.Clamp01(position.y + radius * iy) * size);
                int max_z = Mathf.FloorToInt(Mathf.Clamp01(position.z + radius * iz) * size);
            
                var pos = default(IntVector3);
                pos.z = z;

                var cell = default(Vector3);

                if (pos.z >= min_z && pos.z <= max_z)
                {
                    for (pos.y = min_y; pos.y <= max_y; ++pos.y)
                    {
                        for (pos.x = min_x; pos.x <= max_x; ++pos.x)
                        {
                            cell.x = pos.x * xsize;
                            cell.y = pos.y * ysize;
                            cell.z = pos.z * zsize;

                            float d = Vector3.Distance(cell, rpos);

                            if (d <= radius)
                            {
                                int index = pos.y * size + pos.x;

                                byte p = sampleField[index];
                                byte v = (byte) (1 + d / radius * 254);

                                sampleField[index] = (p == 0 || v < p) ? v : p;//Style.AtomPaletteIndices[i];
                            }
                        }
                    }
                }
            }

            sliceTexture.LoadRawTextureData(sampleField);
            sliceTexture.Apply();
        }

        private void Regenerate()
        {
            if (sliceTexture != null) Destroy(sliceTexture);

            int size = Settings.Resolution.Value;

            sliceTexture = new Texture2D(size,
                size,
                TextureFormat.Alpha8,
                false);
            sliceTexture.filterMode = FilterMode.Point;
            sliceMaterial.SetTexture("_MainTex", sliceTexture);

            sampleField = new byte[size * size];
        }
    }
}
