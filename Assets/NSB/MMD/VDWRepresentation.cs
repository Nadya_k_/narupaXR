﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using NSB.MMD.Base;
using NSB.MMD.RenderingTypes;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Profiling;

namespace NSB.MMD
{
    public class VDWRepresentation : MMDRenderer
    {
        [System.Serializable]
        public class VDWConfig : RendererConfig
        {
            public IntParameter Darken;
            public IntParameter MeshComplexity;
            public BoolParameter Spin;
            public BoolParameter Hemispheres;
            public FloatParameter HemisphereCull;
            public FloatParameter Scaling;

            protected override void Setup()
            {
                Darken = AddInt("Darken", 64, 0, 255);
                MeshComplexity = AddInt(nameof(MeshComplexity), 6, 0, 12);
                Spin = AddBool(nameof(Spin), false);
                Hemispheres = AddBool(nameof(Hemispheres), false);
                HemisphereCull = AddFloat(nameof(HemisphereCull), 0, 0, 1);
                Scaling = AddFloat(nameof(Scaling), 1, dirtying: false);
            }
        }

        [Header("Shaders")]
        [SerializeField]
        private Shader vdwShader;
        [SerializeField]
        private Material vdwMaterial;

        [Header("Setup")]
        public VDWConfig Settings = new VDWConfig();
        public override RendererConfig Config => Settings;

        private ParticleBatchSet particleBatches = new ParticleBatchSet();
        private ParticleBatchRenderer particleRenderer = new ParticleBatchRenderer();

        private Mesh mesh;
        private List<Vector3> rotations = new List<Vector3>();

        private void Awake()
        {
            mesh = new Mesh();

            rotations.Resize(64);
            RandomiseRotations();

            if(vdwMaterial == null)
                vdwMaterial = new Material(vdwShader);
        
        }

        private void RandomiseRotations()
        {
            for (int i = 0; i < rotations.Count; ++i)
            {
                if (!Settings.Hemispheres.Value)
                {
                    rotations[i] = Random.rotationUniform.eulerAngles;
                }
                else
                {
                    rotations[i] = (Quaternion.Euler(-90, 0, 0) * Quaternion.Euler(0, Random.value * 360, 0)).eulerAngles;
                }
            }
        }

        public override void Refresh()
        {
            if (!CheckValidity()) return;

            if (Selection.Count == 0)
            {
                particleRenderer.Clear();
                return;
            }

            if (Settings.ResetDirty()) RegenerateMesh();

            Profiler.BeginSample("Refresh VDWRepresentation");
            if (Settings.Spin.Value)
            {
                RandomiseRotations();
            }

            particleBatches.GenerateParticles(this, Selection, UpdateParticle);
            particleRenderer.RenderBatchSet(particleBatches);
            Profiler.EndSample();
        }

        public static void UpdateParticle(VDWRepresentation data,
            int id,
            ref ParticleSystem.Particle particle)
        {
            var highlighted = data.Style.HighlightedAtoms;

            bool highlight = highlighted != null 
                             && highlighted.Count > 0
                             && !highlighted.Contains(id);
            float mult = highlight ? data.Settings.Darken.Value / 255f : 1;

            particle.position = data.Frame.AtomPositions[id];
            particle.startColor = FasterMath.Mul(data.Style.AtomColors[id], mult);
            particle.startSize3D = FasterMath.Uniform(data.Style.AtomRadiuses[id] * data.Settings.Scaling.Value);
            particle.rotation3D = data.rotations[id % data.rotations.Count];
        }

        private void RegenerateMesh()
        {
            Profiler.BeginSample("Regenerate VDWRepresentation Mesh");
            MeshGeneration.MeshGeneration.GenerateAtomSphere(mesh, 
                Settings.MeshComplexity.Value, 
                hemisphere: Settings.Hemispheres.Value, 
                hemisphereCull: Settings.HemisphereCull.Value);
        
            particleBatches.SetBatchSize(ParticleBatchRenderer.VertexLimit / mesh.vertexCount - 1);
            particleRenderer.Initialise(transform);
            particleRenderer.SetMesh(mesh, billboard: Settings.Hemispheres.Value);
            particleRenderer.SetMaterial(vdwMaterial);

            RandomiseRotations();
            Profiler.EndSample();
        }

        public override float GetAtomBoundingRadius(int id)
        {
            if (Selection == null || Style == null) return -1;

            if (!Selection.Contains(id) || Style.AtomRadiuses.Count <= id) return -1;

            return Style.AtomRadiuses[id] * Settings.Scaling.Value;
        }
    }
}
