﻿using System.Collections.Generic;
using Nano.Science;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Profiling;

namespace NSB.MMD.Styles
{
    public class ColorStyle : FrameFilter, IStyleSource
    {
        public NSBStyle Style = new NSBStyle();

        NSBStyle IStyleSource.Style => Style;

        public List<Color32> ElementColors = new List<Color32>();
        public List<float> ElementRadiuses = new List<float>();

        [Header("Config")]
        [Range(0, 1)]
        public float Hue;

        [Range(0, 1)]
        public float Saturation = 0.65f;

        [Range(0, 1)]
        public float Value = 1f;

        [Range(0, 1)]
        public float Variance;

        [SerializeField]
        private bool refresh;

        [SerializeField]
        private SelectionSource highlightSource;

        private bool colorsChanged;

        protected override void Awake()
        {
            base.Awake();
            ElementColors.Resize((int)Element.MAX);
            ElementRadiuses.Resize((int)Element.MAX);

            RefreshElementStyles();
            Style.Name = name;
        }

        public override void UpdateFilter(NSBFrame frame, bool topologyChanged = false)
        {
            if (refresh)
            {
                RefreshElementStyles();
            }

            if (highlightSource.Linked)
            {
                Style.HighlightedAtoms = highlightSource.Component.Selection;
            }

            if (topologyChanged || colorsChanged)
            {
                RefreshTopology(frame);
            }

            colorsChanged = false;
        }

        public void RefreshTopology(NSBFrame frame)
        {
            Profiler.BeginSample("ColorStyle.UpdateFilter");

            Style.AtomColors.Resize(frame.AtomCount);
            Style.AtomPaletteIndices.Resize(frame.AtomCount);
            Style.AtomRadiuses.Resize(frame.AtomCount);

            for (int i = 0; i < frame.AtomCount; ++i)
            {
                ushort type = frame.AtomTypes[i];

                Style.AtomColors[i] = ElementColors[type];
                Style.AtomPaletteIndices[i] = (byte)type;
                Style.AtomRadiuses[i] = ElementRadiuses[type];
            }

            Style.InitPalette();

            for (int i = 0; i < ElementColors.Count; ++i)
            {
                Style.PaletteColors[i] = ElementColors[i];
            }

            Style.CommitPalette();
            Style.PaletteIsValid = true;

            Profiler.EndSample();
        }

        [ContextMenu("Refresh")]
        public void RefreshElementStyles()
        {
            colorsChanged = true;

            for (int i = 0; i < (int)Element.MAX; ++i)
            {
                var element = (Element)i;

                var properties = PeriodicTable.GetElementProperties(element);

                float s = Mathf.Clamp01(Saturation + (Random.value - 0.5f) * Variance);

                ElementColors[(int)element] = Color.HSVToRGB(Hue, s, Value);
                ElementRadiuses[(int)element] = properties.VDWRadius;
            }
        }
    }
}