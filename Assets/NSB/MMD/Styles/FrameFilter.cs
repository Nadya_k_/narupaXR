﻿using Narupa.VR.Player.Control;
using NSB.Examples.Player.Control;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using UnityEngine;

namespace NSB.MMD.Styles
{
    public class FrameFilter : MonoBehaviour, IRefreshable
    {
        public bool AutoRefresh = true;

        [SerializeField]
        protected FrameSource frameSource;

        protected int lastTopologyCount = -1;

        protected virtual void Awake()
        {
            PlaybackRenderer pb = FindObjectOfType<PlaybackRenderer>();
            if (pb != null) pb.OnReady += OnReady;
            else
            {
                //@mark @moc @review how to deal with this elegantly?
                VrPlaybackRenderer playback = FindObjectOfType<VrPlaybackRenderer>();
                if(playback != null) playback.OnReady += OnReady;
            }
        }

        protected virtual void OnReady()
        {
            lastTopologyCount = -1;
        }

        private void Update()
        {
            if (AutoRefresh) Refresh();
        }

        public virtual void Refresh()
        {
            var frame = frameSource.Component.Frame;

            UpdateFilter(frameSource.Component.Frame, lastTopologyCount != frame.TopologyUpdateCount);

            lastTopologyCount = frame.TopologyUpdateCount;
        }

        public virtual void UpdateFilter(NSBFrame frame, bool topologyChanged = false)
        {
        }
    }
}