﻿using System.Collections.Generic;
using NSB.MMD.Base;
using NSB.MMD.RenderingTypes;
using NSB.Processing;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Profiling;

namespace NSB.MMD.Styles
{
    public class MaxRadiusStyle : FrameFilter, IStyleSource
    {
        public NSBStyle Style = new NSBStyle();

        NSBStyle IStyleSource.Style => Style;

        [Header("Config")]
        public Color32 AtomColor;
        [Range(0.5f, 2)]
        public float Scale = 1;
        [Range(0, 0.1f)]
        public float Boost;
        public List<MMDRenderer> Renderers = new List<MMDRenderer>();

        public override void UpdateFilter(NSBFrame frame, bool topologyChanged = false)
        {
            Profiler.BeginSample("MaxRadiusStyle.UpdateFilter");

            Style.AtomColors.Resize(frame.AtomCount);
            Style.AtomPaletteIndices.Resize(frame.AtomCount);
            Style.AtomRadiuses.Resize(frame.AtomCount);

            for (int i = 0; i < frame.AtomCount; ++i)
            {
                Style.AtomRadiuses[i] = 0;
                Style.AtomColors[i] = AtomColor;
            }

            foreach (var renderer in Renderers)
            {
                if (!renderer.Valid)
                    continue;
                for (int i = 0; i < frame.AtomCount; ++i)
                {
                    Style.AtomRadiuses[i] = Mathf.Max(Style.AtomRadiuses[i], renderer.GetAtomBoundingRadius(i));
                }
            }

            for (int i = 0; i < frame.AtomCount; ++i)
            {
                Style.AtomRadiuses[i] *= Scale;
                Style.AtomRadiuses[i] += Boost;
            }

            Profiler.EndSample();
        }
    }
}
