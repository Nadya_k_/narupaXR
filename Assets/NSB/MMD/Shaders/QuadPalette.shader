﻿Shader "NSB/Quad Palette"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Palette ("Palette", 2D) = "white" {}
		_Sharpness ("Sharpness", Range(0, 1)) = .5
	}
	SubShader
	{
		Tags
		{ 
			"Queue"="Geometry" 
			"RenderType"="Opaque" 
			"PreviewType"="Plane"
		}

		Cull Off
		Lighting Off
		ZWrite On

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			#include "DrawingPalette.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

			sampler2D _MainTex;
			sampler2D _Palette;
			float4 _MainTex_ST;
			fixed _Sharpness;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.color = v.color;

				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				half4 tex = tex2D(_MainTex, i.uv);

				if (tex.a == 0) discard;

				return pal2Dindex(_Palette, tex.a);
			}
			ENDCG
		}
	}
}
