Shader "NSB/MMD/VDW"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
		_Transparency ("Transparency", float) = 1
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Geometry+10" 
			"IgnoreProjector"="True" 
			"RenderType"="Opaque" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Tags{ "LightMode" = "ForwardBase" }

		Cull Back
		Lighting Off
		ZWrite On

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile _ PIXELSNAP_ON
			#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
			#pragma multi_compile_fog
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				fixed4 diff : COLOR0; // diffuse lighting color
				float4 vertex : SV_POSITION;

				//float4 screenPos : TEXCOORD1;

				UNITY_FOG_COORDS(1)
			};
			
			fixed4 _Color;
			float _Transparency;

			v2f vert(appdata_full IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.diff = IN.color;

				/*
				// get view direction in world space
				float3 worldViewDir = normalize(UnityWorldSpaceViewDir(IN.vertex));

				// get vertex normal in world space
				half3 worldNormal = UnityObjectToWorldNormal(IN.normal);

				// dot product between normal and view direction 
				float nl = max(0, dot(worldNormal, worldViewDir));
			
				// calculate color 
				OUT.diff = lerp(
					// flat color
					fixed4(IN.color.rgb, 1),
					// soft light color 
					fixed4(_Color.rgb * IN.color.rgb, (1 - nl) + 1),
					// blending factor 
					IN.color.a
				);
				*/

				//OUT.screenPos = ComputeScreenPos(OUT.vertex);

				UNITY_TRANSFER_FOG(OUT, OUT.vertex);

				return OUT;
			}

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = IN.diff;
				
				c.rgb = clamp(c.rgb * c.a, 0, 1);

				/*
				float4x4 thresholdMatrix =
				{  1.0 / 17.0,  9.0 / 17.0,  3.0 / 17.0, 11.0 / 17.0,
				  13.0 / 17.0,  5.0 / 17.0, 15.0 / 17.0,  7.0 / 17.0,
				   4.0 / 17.0, 12.0 / 17.0,  2.0 / 17.0, 10.0 / 17.0,
				  16.0 / 17.0,  8.0 / 17.0, 14.0 / 17.0,  6.0 / 17.0
				};
				*/
				//float4x4 _RowAccess = { 1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 };
				//float2 pos = IN.screenPos.xy / IN.screenPos.w;
				//pos *= _ScreenParams.xy; // pixel position
				//clip(_Transparency * c.a - thresholdMatrix[fmod(pos.x, 4)] * _RowAccess[fmod(pos.y, 4)]);

				c.a = 1;

				UNITY_APPLY_FOG(IN.fogCoord, c);
				UNITY_OPAQUE_ALPHA(c.a);

				return c;
			}
		ENDCG
		}
	}
}
