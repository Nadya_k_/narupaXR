﻿Shader "NSB/Colored Line Ribbon"
{
    Properties
    {
		_Scaling ("Scaling", Range(0, 1)) = 0.02
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Geometry"
            "IgnoreProjector" = "True"
            "RenderType" = "Opaque"
            "PreviewType" = "Plane"
        }

        Cull Off
        Lighting Off
        ZWrite On

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma geometry geom
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
				float4 color : COLOR;
				float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
				float4 color : COLOR;
				float3 normal : NORMAL;
            };

			float _Scaling;

			v2f vert(appdata v)
			{
				v2f o;

				o.pos =  mul(unity_ObjectToWorld, v.vertex);
				o.color = v.color;
				o.normal = mul(unity_ObjectToWorld, v.normal);

				return o;
			}

            [maxvertexcount(4)]
            void geom(line v2f p[2], inout TriangleStream<v2f> triStream)
            {
                float4 v0 = float4(p[0].pos + p[0].normal * _Scaling, 1.0);
                float4 v1 = float4(p[0].pos - p[0].normal * _Scaling, 1.0);
                float4 v2 = float4(p[1].pos + p[1].normal * _Scaling, 1.0);
                float4 v3 = float4(p[1].pos - p[1].normal * _Scaling, 1.0);
 
 				v0 = mul(unity_WorldToObject, v0);
				v1 = mul(unity_WorldToObject, v1);
				v2 = mul(unity_WorldToObject, v2);
				v3 = mul(unity_WorldToObject, v3);

				v2f pIn = p[0];
                pIn.pos = UnityObjectToClipPos(v0);
                pIn.color = p[0].color;
                triStream.Append(pIn);
 
                pIn.pos = UnityObjectToClipPos(v1);
                pIn.color = p[0].color;
                triStream.Append(pIn);
 
                pIn.pos = UnityObjectToClipPos(v2);
				pIn.color = p[1].color;
                triStream.Append(pIn);
 
                pIn.pos = UnityObjectToClipPos(v3);
				pIn.color = p[1].color;
                triStream.Append(pIn);
            }

            fixed4 frag(v2f i) : SV_Target
			{
				return i.color;
			}
            ENDCG
        }
    }
}
