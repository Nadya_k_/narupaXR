﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.MMD.Base;
using NSB.Utility;
using UnityEngine;

namespace NSB.MMD
{
    public class AtomTrailRenderer : MMDRenderer
    {
        [System.Serializable]
        public class TrailConfig : RendererConfig
        {
            public FloatParameter Scaling;

            protected override void Setup()
            {
                Scaling = AddFloat("Scaling", .333f, 0, 2, dirtying: false);
            }
        }

        [Header("Setup")]
        [SerializeField]
        private AnimationCurve widthCurve;
        [SerializeField]
        private TrailRenderer trailTemplate;
        private IndexedPool<TrailRenderer> trails;

        public TrailConfig Settings = new TrailConfig();
        public override RendererConfig Config => Settings;

        private static Gradient gradient = new Gradient();
        private static GradientColorKey[] gck = new GradientColorKey[2];
        private static GradientAlphaKey[] gak = new GradientAlphaKey[2];

        private void Awake()
        {
            trails = new IndexedPool<TrailRenderer>(trailTemplate, InstanceSetup: Setup);

            gck[0].time = 0.0F;
            gck[1].color = Color.black;
            gck[1].time = 1.0F;

            gak[0].alpha = 1.0F;
            gak[0].time = 0.0F;
            gak[1].alpha = 1.0f;
            gak[1].alpha = 0.0F;
            gak[1].time = 1.0F;
        }

        private void Setup(TrailRenderer trail)
        {
            trail.Clear();
        }

        public override void Refresh()
        {
            trails.SetActive(Frame.AtomCount);
            trails.MapActive((i, trail) =>
            {
                Color32 color = Style.AtomColors[i];

                //gak[0].alpha = selected ? 1.0F : 0;

                gck[0].color = color;
                gck[1].color = color;
                gradient.SetKeys(gck, gak);
                trail.colorGradient = gradient;

                trail.transform.localPosition = Frame.AtomPositions[i];
                trail.widthMultiplier = Settings.Scaling.Value;
                trail.widthCurve = widthCurve;
            });
        }
    }
}
