﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using UnityEngine;

namespace NSB.MMD.RenderingTypes
{
    public class RefreshList : MonoBehaviour, IRefreshable
    {
        [SerializeField]
        private bool autoRefresh = true;

        [SerializeField]
        private List<Refreshable> refreshables;

        private void Update()
        {
            if (autoRefresh) Refresh();
        }

        public void Refresh()
        {
            UnityEngine.Profiling.Profiler.BeginSample("Refresh RefreshList");

            for (int i = 0; i < refreshables.Count; ++i)
            {
                refreshables[i].Component.Refresh();
            }

            UnityEngine.Profiling.Profiler.EndSample();
        }
    }
}
