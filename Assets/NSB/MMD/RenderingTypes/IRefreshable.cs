﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.Utility;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace NSB.MMD.RenderingTypes
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(Refreshable))]
    public class RefreshableDrawer : InterfaceInspectorLinkDrawer<IRefreshable> { }

#endif
    public interface IRefreshable 
    {
        void Refresh();
    }

    [System.Serializable]
    public class Refreshable : InterfaceInspectorLink<IRefreshable> { }
}