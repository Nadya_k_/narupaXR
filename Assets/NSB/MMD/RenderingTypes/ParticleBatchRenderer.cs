﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.Utility;
using UnityEngine;

namespace NSB.MMD.RenderingTypes
{
    public class ParticleBatchRenderer
    {
        public const int VertexLimit = 521200; // seems some guesswork is needed here

        private Transform parent;
        private bool billboard;
        private Mesh mesh;
        private Material material;
        private IndexedPool<ParticleSystem> particleSystems;

        public ParticleSystem CreateSystem()
        {
            var obj = new GameObject("Particle System", typeof(ParticleSystem));
            obj.transform.SetParent(parent, false);
            obj.layer = parent.gameObject.layer;

            var system = obj.GetComponent<ParticleSystem>();
            system.Stop();
            var main = system.main;
            main.duration = 0f;
            main.scalingMode = ParticleSystemScalingMode.Hierarchy;
            main.loop = false;
            var emisssion = system.emission;
            emisssion.enabled = false;
            var shape = system.shape;
            shape.enabled = false;

            var renderer = obj.GetComponent<ParticleSystemRenderer>();
            renderer.renderMode = ParticleSystemRenderMode.Mesh;
            renderer.alignment = billboard
                ? ParticleSystemRenderSpace.Facing
                : ParticleSystemRenderSpace.Local;
            renderer.mesh = mesh;
            renderer.material = material;

            system.Play();

            return system;
        }

        public void Initialise(Transform transform)
        {
            if (particleSystems != null) return;

            parent = transform;
            particleSystems = new IndexedPool<ParticleSystem>(() => CreateSystem(), transform);
        }

        public void SetMesh(Mesh mesh, bool billboard=false)
        {
            this.mesh = mesh;
            this.billboard = billboard;

            particleSystems?.MapAll((i, system) =>
            {
                var renderer = system.GetComponent<ParticleSystemRenderer>();

                renderer.renderMode = ParticleSystemRenderMode.Mesh;
                renderer.mesh = mesh;
                renderer.alignment = billboard
                    ? ParticleSystemRenderSpace.Facing
                    : ParticleSystemRenderSpace.Local;
            });
        }

        public void SetMaterial(Material material)
        {
            this.material = material;

            particleSystems?.MapAll((i, system) =>
            {
                system.GetComponent<ParticleSystemRenderer>().sharedMaterial = material;
            });
        }

        public void Clear()
        {
            particleSystems?.SetActive(0);
        }

        public void RenderBatchSet(ParticleBatchSet batches)
        {
            UnityEngine.Profiling.Profiler.BeginSample("ParticleBatchRenderer.RenderBatchSet");
            particleSystems.SetActive(batches.BatchCount);

            if (batches.BatchCount == 0)
            {
                return;
            }

            // update all batches, treating the last one as an edge case because
            // it doesn't use all the particles in the batch
            int lastBatch = batches.BatchCount - 1;

            for (int batch = 0; batch < lastBatch; ++batch)
            {
                particleSystems[batch].SetParticles(batches.Batches[batch],
                    batches.BatchSize);
            }

            particleSystems[lastBatch].SetParticles(batches.Batches[lastBatch],
                batches.LastBatchRemainder);

            UnityEngine.Profiling.Profiler.EndSample();
        }
    }
}
