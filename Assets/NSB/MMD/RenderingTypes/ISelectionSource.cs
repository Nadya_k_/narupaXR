﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.Processing;
using NSB.Utility;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace NSB.MMD.RenderingTypes
{
#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(SelectionSource))]
    public class SelectionSourceDrawer : InterfaceInspectorLinkDrawer<ISelectionSource> { }
#endif

    public interface ISelectionSource 
    {
        NSBSelection Selection { get; }
    }

    [System.Serializable]
    public class SelectionSource : InterfaceInspectorLink<ISelectionSource> { }
}