﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using NSB.Utility;
using UnityEngine;
using UnityEngine.Assertions;

namespace NSB.MMD.RenderingTypes
{
    public class ParticleBatchSet
    {
        public List<ParticleSystem.Particle[]> Batches
            = new List<ParticleSystem.Particle[]>();

        public delegate void ParticleUpdate<TContext, TData>(TContext context,
            TData data, 
            ref ParticleSystem.Particle particle);

        public int ParticleCount;
        public int BatchSize;

        public int BatchCount;
        public int LastBatchRemainder;

        public ParticleSystem.Particle[] this[int index]
        {
            get
            {
                return Batches[index];
            }
        }

        public void SetBatchSize(Mesh mesh)
        {
            SetBatchSize(ParticleBatchRenderer.VertexLimit / mesh.vertexCount);
        }

        public void SetBatchSize(int batchSize)
        {
            if (batchSize != 0 && batchSize != BatchSize)
            {
                BatchSize = batchSize == 0 ? BatchSize : batchSize;
                RefreshBatches();
            }
        }

        public void SetParticleCount(int count, int batchSize = 0)
        {
            if (count != ParticleCount || (batchSize != 0 && batchSize != BatchSize))
            {
                ParticleCount = count;
                BatchSize = batchSize == 0 ? BatchSize : batchSize;
                RefreshBatches();
            }
        }

        public void GenerateParticles<TContext>(TContext context,
            int count,
            ParticleUpdate<TContext, int> update)
        {
            SetParticleCount(count);

            int batch = 0;
            int index = 0;

            for (int id = 0; id < count; ++id)
            {
                update(context, id, ref Batches[batch][index]);

                index += 1;

                if (index == BatchSize)
                {
                    index = 0;
                    batch += 1;
                }
            }
        }

        public void GenerateParticles<TContext, TData>(TContext context, 
            IList<TData> data, 
            ParticleUpdate<TContext, TData> update)
        {
            Assert.IsTrue(Batches.Count > 0, "Can't generate particles into no batches!");

            int count = data.Count;

            SetParticleCount(count);

            int batch = 0;
            int index = 0;

            for (int i = 0; i < count; ++i)
            {
                update(context, data[i], ref Batches[batch][index]);

                index += 1;

                if (index == BatchSize)
                {
                    index = 0;
                    batch += 1;
                }
            }
        }

        private void RefreshBatches()
        {
            BatchCount = Mathf.Max(1, Mathf.CeilToInt(ParticleCount / (float) BatchSize));

            Batches.Resize(BatchCount);
        
            for (int i = 0; i < BatchCount; ++i)
            {
                if (Batches[i] == null || Batches[i].Length < BatchSize)
                {
                    Batches[i] = new ParticleSystem.Particle[BatchSize];
                }
            }

            LastBatchRemainder = BatchCount > 0
                ? (ParticleCount % BatchSize)
                : 0;
        }
    }
}
