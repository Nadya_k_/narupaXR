﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.MMD.Base;
using NSB.Utility;
using UnityEngine;

namespace NSB.MMD.Renders
{
    public class TransformFollowAtomCentroid : MMDRenderer
    {
        public float FollowTime = 1f;

        public override RendererConfig Config => null;

        private Vector3 target;
        private Vector3 velocity;

        public override void Refresh()
        {
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition,
                target,
                ref velocity,
                FollowTime);

            if (Frame == null || Frame.AtomCount == 0) return;
            if (Selection == null || Selection.Count == 0) return;

            var centroid = Vector3.zero;

            for (int i = 0; i < Selection.Count; ++i)
            {
                centroid = FasterMath.Add(centroid, Frame.AtomPositions[Selection[i]]);
            }

            target = centroid / Selection.Count;
        }
    }
}
