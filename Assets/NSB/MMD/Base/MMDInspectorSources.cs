﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using NSB.MMD.RenderingTypes;
using UnityEngine;

namespace NSB.MMD.Base
{
    /// <summary>
    /// Component to facilitate linking renderers through Unity's inspector / heirarchy
    /// instead of through code.
    /// 
    /// Retrieves the sibling IMMDRenderer component and continuously set its frame, 
    /// selection, and style from the linked sources. 
    /// </summary>
    public class MMDInspectorSources : MonoBehaviour 
    {
        [Header("Sources")]
        public FrameSource FrameSource;
        public SelectionSource SelectionSource;
        public StyleSource StyleSource;
        public bool IgnoreSources;

        public IMMDRenderer Renderer => GetComponent<IMMDRenderer>();

        private void Update()
        {
            if (IgnoreSources) return;

            var renderer = Renderer;

            if (    FrameSource.Linked) renderer.Frame     =     FrameSource.Component.Frame;
            if (SelectionSource.Linked) renderer.Selection = SelectionSource.Component.Selection;
            if (    StyleSource.Linked) renderer.Style     =     StyleSource.Component.Style;
        }
    }
}
