﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace NSB.MMD.Base
{
    #if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(SectionAttribute))]
    public class SectionAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            position.y += 10;
            property.isExpanded = true;
            //EditorGUI.PropertyField(position, property, GUIContent.none, true);

            ///*
            while (property.NextVisible(true))
            {
                position.height = 16;
                EditorGUI.PropertyField(position, property, false);
                position.y += 18; // GetPropertyHeight(property, GUIContent.none);
            }
            //*/

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            property.isExpanded = true;

            return EditorGUI.GetPropertyHeight(property, true) + 10;
        }
    }
    #endif

    public class SectionAttribute : PropertyAttribute { }
}