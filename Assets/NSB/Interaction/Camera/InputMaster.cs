﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace NSB.Interaction.Camera
{
    enum InputType
    {
        None, 
        Disabled,
        ForceField, 
        RaySelect, 
        ValueChange, 
        Camera, 
        PlaceAtom
    }

    static class InputMaster
    {
        public static InputType CurrentInput { get; private set; } 

        public static bool Enabled
        {
            get { return CurrentInput != InputType.Disabled; }
            set
            {
                if (Enabled == false && value)
                {
                    CurrentInput = InputType.None;
                }
            }
        }

        public static void ReleaseInputType(InputType type)
        {
            if (CurrentInput == type)
            {
                CurrentInput = InputType.None; 
            }
        }

        public static bool CanChangeInputTypeTo(InputType newType)
        {
            if (CurrentInput == newType)
            {
                return true; 
            }

            switch (CurrentInput)
            {
                case InputType.None:
                    return true;
                case InputType.Disabled:
                    return false;
                case InputType.ForceField:
                    switch (newType)
                    {
                        case InputType.None:
                            return true;
                        case InputType.PlaceAtom:
                            return true;
                        default:
                            return false;
                    }
                case InputType.PlaceAtom:
                    switch (newType)
                    {
                        case InputType.Camera:
                            return false;
                        default:
                            return true;
                    }
                case InputType.RaySelect:
                    switch (newType)
                    {
                        case InputType.None:
                            return true;
                        case InputType.PlaceAtom:
                            return true;
                        case InputType.ForceField:
                            return true;
                        case InputType.RaySelect:
                            return true;
                        case InputType.ValueChange:
                            return true;
                        case InputType.Camera:
                            return true;
                        default:
                            return false;
                    }
                case InputType.ValueChange:
                    switch (newType)
                    {
                        case InputType.None:
                            return true;
                        case InputType.PlaceAtom:
                            return true;
                        default:
                            return false;
                    }
                case InputType.Camera:
                    switch (newType)
                    {
                        case InputType.None:
                            return true;
                        case InputType.PlaceAtom:
                            return true;
                        default:
                            return false;
                    }  
                default:
                    return false;
            }
        }

        public static void ReleaseAllInputTypes()
        {
            CurrentInput = InputType.None; 
        }

        public static bool TryChangeInputTypeTo(InputType newType)
        {
            if (CanChangeInputTypeTo(newType) == false)
            {
                return false;
            }

            CurrentInput = newType;

            return true;
        }
    }
}
