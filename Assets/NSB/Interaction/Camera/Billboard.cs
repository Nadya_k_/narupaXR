﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Interaction.Camera
{
    class Billboard : MonoBehaviour
    {
        public new UnityEngine.Camera camera;

        void Update()
        {
            if (camera == null)
            {
                camera = UnityEngine.Camera.main;
            }

            transform.LookAt(camera.transform.position, camera.transform.up);
        }
    }
}