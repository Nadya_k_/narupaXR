﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Interaction.Camera
{
    abstract class IInteraction : MonoBehaviour
    {
        public bool Enabled { get; set; } 

        public abstract bool UpdateInteractions();
    }
}
