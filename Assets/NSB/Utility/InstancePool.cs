﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Profiling;

namespace NSB.Utility
{
    public abstract class InstancePoolBase<TConfig, TInstance>
        where TInstance : IConfigView<TConfig>
    {
        /// <summary>
        /// Mapping of active configs to the corresponding active instance
        /// </summary>
        protected Dictionary<TConfig, TInstance> instances
            = new Dictionary<TConfig, TInstance>();
    
        /// <summary>
        /// Pool of inactive instances for later reuse
        /// </summary>
        protected List<TInstance> spare = new List<TInstance>();

        /// <summary>
        /// Create a new instance from scratch.
        /// </summary>
        protected abstract TInstance CreateNew();

        /// <summary>
        /// Find an instance to configure with the given config data. If there are
        /// no spares then create a new one.
        /// </summary>
        protected TInstance FindNew(TConfig config)
        {
            TInstance instance;

            // if there is a spare instance, use that instead of creating a new one
            if (spare.Count > 0)
            {
                instance = spare[spare.Count - 1];
                spare.RemoveAt(spare.Count - 1);
            }
            else
            {
                instance = CreateNew();
            }

            // associate this config with this instance
            Configure(config, instance);

            return instance;
        }

        /// <summary>
        /// Get the corresponding instance for the given config data. If this 
        /// config wasn't already active, make it active and find an instance for
        /// it.
        /// </summary>
        public TInstance Get(TConfig config)
        {
            TInstance instance;

            if (!instances.TryGetValue(config, out instance))
            {
                instance = FindNew(config);
            }

            return instance;
        }

        /// <summary>
        /// Ensure the given config and corresponding instance are no longer
        /// active, returning true if any action had to be taken to deactive them
        /// </summary>
        public bool Discard(TConfig config)
        {
            TInstance instance;

            if (instances.TryGetValue(config, out instance))
            {
                // remove the instance from be associated with a config and add it
                // to the pool of spare instances
                instances.Remove(config);
                spare.Add(instance);

                Cleanup(config, instance);

                return true;
            }

            return false;
        }

        /// <summary>
        /// Deactivate all configs and instances
        /// </summary>
        public void Clear()
        {
            foreach (var pair in instances)
            {
                Cleanup(pair.Key, pair.Value);
            }

            spare.AddRange(instances.Values);
            instances.Clear();
        }

        /// <summary>
        /// Associate the given instance as corresponding to the given config,
        /// and pass the config data to the instance.
        /// </summary>
        protected virtual void Configure(TConfig config, TInstance instance)
        {
            // record this instance as corresponding to this config
            instances.Add(config, instance);
            // update the instance to reflect the new config
            instance.SetConfig(config);
        }

        /// <summary>
        /// Reset the given instance to be reused later 
        /// </summary>
        protected virtual void Cleanup(TConfig config, TInstance instance)
        {
            instance.Cleanup();
        }

        /// <summary>
        /// Call `Refresh` on all active instances
        /// </summary>
        public void Refresh()
        {
            MapActive(i => i.Refresh());
        }

        /// <summary>
        /// Set all given configs active i.e ensure an active instance exists that
        /// corresponds to each one
        /// </summary>
        public void SetActive(params TConfig[] configs)
        {
            SetActive(configs as IEnumerable<TConfig>);
        }

        // intermediate stores for efficiently switching which instances are active
        [ThreadStatic]
        private HashSet<TConfig> prev = new HashSet<TConfig>();
        [ThreadStatic]
        private HashSet<TConfig> next = new HashSet<TConfig>();
        [ThreadStatic]
        private List<TConfig> added = new List<TConfig>();

        /// <summary>
        /// Variant of SetActiveList that allocates less memory, only works when
        /// the active configs are provided as an IList
        /// </summary>
        private bool SetActiveIList(IList<TConfig> active)
        {
            if (active == null) return false;

            Profiler.BeginSample("InstancePoolBase.SetActiveIList");

            int count = active.Count;

            // work out which shortcuts should be active next, removing them from the
            // previous set of shortcuts to indicate they don't need to be discarded
            for (int i = 0; i < count; ++i)
            {
                TConfig shortcut = active[i];

                // if a shortcut couldn't be removed from the previous shortcuts then it
                // was not present and so needs to be added (but 
                if (!prev.Remove(shortcut))
                {
                    added.Add(shortcut);
                }

                next.Add(shortcut);
            }

            // every shortcut from the previous set that was not removed must not be in
            // the next set and so needs to be discarded
            foreach (var shortcut in prev)
            {
                Discard(shortcut);
            }

            prev.Clear();

            // now we've freed up the unused instances, activate all the newly added
            // elements
            foreach (var shortcut in added)
            {
                FindNew(shortcut);
            }

            added.Clear();

            // the next set becomes the previous set
            var temp = prev;
            prev = next;
            next = temp;

            Profiler.EndSample();

            return true;
        }

        /// <summary>
        /// Set all given configs active i.e ensure an active instance exists that
        /// corresponds to each one
        /// </summary>
        public virtual void SetActive(IEnumerable<TConfig> active)
        {
            // if the collection of configs is an IList we can use a more efficient
            // method
            if (SetActiveIList(active as IList<TConfig>)) return;

            Profiler.BeginSample("InstancePoolBase.SetActive");

            // work out which shortcuts should be active next, removing them from the
            // previous set of shortcuts to indicate they don't need to be discarded
            foreach (var shortcut in active)
            {
                // if a shortcut couldn't be removed from the previous shortcuts then it
                // was not present and so needs to be added
                if (!prev.Remove(shortcut))
                {
                    added.Add(shortcut);
                }

                next.Add(shortcut);
            }

            // every shortcut from the previous set that was not removed must not be in
            // the next set and so needs to be discarded
            foreach (var shortcut in prev)
            {
                Discard(shortcut);
            }

            prev.Clear();

            // now we've freed up the unused instances, activate all the newly added
            // elements
            foreach (var shortcut in added)
            {
                FindNew(shortcut);
            }

            added.Clear();

            // the next set becomes the previous set
            var temp = prev;
            prev = next;
            next = temp;

            Profiler.EndSample();
        }

        /// <summary>
        /// Iterate each active instance in this pool and invoke the given callback 
        /// with that instance.
        /// </summary>
        public void MapActive(Action<TInstance> action)
        {
            foreach (TInstance instance in instances.Values)
            {
                action(instance);
            }
        }

        /// <summary>
        /// Return true if the given config is active i.e has a corresponding
        /// active instance in this pool
        /// </summary>
        public bool IsActive(TConfig shortcut)
        {
            return instances.ContainsKey(shortcut);
        }

        /// <summary>
        /// Invoke the given callback on the instance corresponding to the given
        /// config data if that config is active and has a corresponding instance,
        /// returning true if the callback was invoked.
        /// </summary>
        public bool DoIfActive(TConfig shortcut,
            Action<TInstance> action)
        {
            TInstance instance;

            if (instances.TryGetValue(shortcut, out instance))
            {
                action(instance);

                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// Conveniece object designed for linking a pool's prefab and parent on the
    /// Unity component inspector. 
    /// 
    /// Does not use type generics because Unity is unable to serialize/show 
    /// inspectors for generic types. Instead generic Finalise methods are provided
    /// that return the appropriate type of pool.
    /// </summary>
    [Serializable]
    public class InstancePoolSetup
    {
        /// <summary>
        /// The GameObject to be used as the prefab to create new instances from
        /// </summary>
        public GameObject prefab;

        /// <summary>
        /// The Transform to be used as the parent to newly created instances
        /// </summary>
        public Transform parent;

        /// <summary>
        /// Return a reference to the Prefab's Component of type TInstance, logging
        /// an error if it does not exist.
        /// </summary>
        private TInstance GetConcretePrefab<TInstance>()
            where TInstance : class
        {
            var prefab = this.prefab.GetComponent<TInstance>();

            Assert.IsNotNull(prefab, string.Format("Prefab {0} is missing {1} component",
                this.prefab.name,
                typeof(TInstance).Name));

            return prefab;
        }

        /// <summary>
        /// Create and assign a new IndexedPool to the referenced member
        /// 
        /// This form exists to allow c# to infer the generic types and avoid
        /// writing them out
        /// </summary>
        public void Finalise<TInstance>(ref IndexedPool<TInstance> pool)
            where TInstance : Component => pool = Finalise<TInstance>();

        /// <summary>
        /// Create and assign a new InstancePool to the referenced member.
        /// Optionally, sorting of GameObjects based on SetActive order can be
        /// disabled.
        /// 
        /// This form exists to allow c# to infer the generic types and avoid
        /// writing them out
        /// </summary>
        public void Finalise<TConfig>(ref InstancePool<TConfig> pool, bool sort = true) => pool = Finalise<TConfig>(sort);

        /// <summary>
        /// Create and assign a new InstancePool to the referenced member.
        /// Optionally, sorting of GameObjects based on SetActive order can be
        /// disabled.
        /// 
        /// This form exists to allow c# to infer the generic types and avoid
        /// writing them out
        /// </summary>
        public void Finalise<TConfig, TInstance>(ref InstancePool<TConfig, TInstance> pool, bool sort = true)
            where TInstance : InstanceView<TConfig> => pool = Finalise<TConfig, TInstance>(sort);

        public InstancePool<TConfig> FinaliseAudio<TConfig>(bool sort = true, IEqualityComparer<TConfig> comparer = null)
        {
            var prefab = GetConcretePrefab<InstanceView<TConfig>>();

            return new InstancePool<TConfig>(prefab, parent, sort: sort, comparer: comparer);
        }

        public IndexedPool<TInstance> Finalise<TInstance>()
            where TInstance : Component
        {
            var prefab = GetConcretePrefab<TInstance>();

            return new IndexedPool<TInstance>(prefab, parent);
        }

        /// <summary>
        /// Create and return a new InstancePool using the inspector-assigned
        /// prefab and parent.Optionally, sorting of GameObjects based on 
        /// SetActive order can be disabled.
        /// </summary>
        public InstancePool<TConfig> Finalise<TConfig>(bool sort = true)
        {
            var prefab = GetConcretePrefab<InstanceView<TConfig>>();

            return new InstancePool<TConfig>(prefab, parent, sort: sort);
        }

        /// <summary>
        /// Create and return a new InstancePool using the inspector-assigned
        /// prefab and parent. Optionally, sorting of GameObjects based on 
        /// SetActive order can be disabled.
        /// </summary>
        public InstancePool<TConfig, TInstance> Finalise<TConfig, TInstance>(bool sort = true)
            where TInstance : InstanceView<TConfig>
        {
            var prefab = GetConcretePrefab<TInstance>();

            return new InstancePool<TConfig, TInstance>(prefab, parent, sort: sort);
        }

        public void Finalise<TInstance>(ref SimplePool<TInstance> pool)
            where TInstance : Component
        {
            var prefab = GetConcretePrefab<TInstance>();

            pool = new SimplePool<TInstance>(prefab, parent);
        }
    }

    /// <summary>
    /// TODO: i'm (mark) very confused why i made this this way
    /// </summary>
    public class InstancePool<TConfig, TInstance> : InstancePoolBase<TConfig, TInstance>
        where TInstance : InstanceView<TConfig>
    {
        protected TInstance prefab;
        protected Transform parent;
        protected bool sort;

        public InstancePool(TInstance prefab,
            Transform parent,
            bool sort = true)
        {
            this.prefab = prefab;
            this.parent = parent;
            this.sort = sort;
        }

        /// <summary>
        /// Create and return a new TInstance by instantiating a new prefab
        /// and calling its Setup method
        /// </summary>
        protected override TInstance CreateNew()
        {
            TInstance instance = UnityEngine.Object.Instantiate(prefab);

            instance.Setup();

            return instance;
        }

        /// <summary>
        /// Associate the given instance as corresponding to the given config,
        /// and pass the config data to the instance.
        /// 
        /// Because we are working with GameObjects we set them active and 
        /// reparent.
        /// 
        /// TODO: shouldn't reparenting be done as a one-time Setup for each 
        /// instance?
        /// </summary>
        protected override void Configure(TConfig config, TInstance instance)
        {
            instance.transform.SetParent(parent, false);
            instance.gameObject.SetActive(true);

            base.Configure(config, instance);
        }

        /// <summary>
        /// Reset the given instance to be reused later 
        /// 
        /// Because we are working with GameObjects we set them inactive to hide
        /// them in the scene
        /// </summary>
        protected override void Cleanup(TConfig config, TInstance instance)
        {
            instance.gameObject.SetActive(false);

            base.Cleanup(config, instance);
        }

        /// <summary>
        /// Set all given configs active i.e ensure an active instance exists that
        /// corresponds to each one. Then, if sorting is enabled, the GameObject
        /// heirarchy order is sorted to match the order the configs were provided
        /// </summary>
        public override void SetActive(IEnumerable<TConfig> active)
        {
            base.SetActive(active);

            if (sort)
            {
                foreach (TConfig shortcut in active)
                {
                    Get(shortcut).transform.SetAsLastSibling();
                }
            }
        }
    }

    /// <summary>
    /// InstancePool variant where TInstance is assumed to be InstanceView<TConfig>
    /// 
    /// TODO: well isn't that already a constraint on InstancePool?
    /// </summary>
    public class InstancePool<TConfig> : InstancePool<TConfig, InstanceView<TConfig>>
    {
        public InstancePool(InstanceView<TConfig> prefab, Transform parent, bool sort = true, IEqualityComparer<TConfig> comparer = null)
            : base(prefab, parent, sort)
        {
            instances = new Dictionary<TConfig, InstanceView<TConfig>>(comparer);
        }
    }

//public class InstancePool<TConfig> : InstancePool<TConfig, InstanceView<TConfig>>
//{
//    public InstancePool(InstanceView<TConfig> prefab, Transform parent, bool sort = true)
//        : base(prefab, parent, sort)
//    {
//    }
//}
}