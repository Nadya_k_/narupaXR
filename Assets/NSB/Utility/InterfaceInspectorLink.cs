﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace NSB.Utility
{
#if UNITY_EDITOR
    public class InterfaceInspectorLinkDrawer<TInterface> : PropertyDrawer
        where TInterface : class
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.PropertyField(position, property.FindPropertyRelative("link"), new GUIContent(property.displayName));
        }
    }
#endif

    [System.Serializable]
    public class InterfaceInspectorLink<TInterface>
        where TInterface : class
    {
        [SerializeField]
        private GameObject link;

        private GameObject prev;

        public bool Linked => link != null;

        private TInterface _component;

        public TInterface Component
        {
            get
            {
                Refresh();

                return _component;
            }
        }

        public void Refresh()
        {
            if (link == null)
            {
                Debug.LogErrorFormat($"Missing GameObject on InterfaceInspectorLink<{typeof(TInterface).Name}>!");
            }

            if (link != prev)
            {
                prev = link;
                _component = link.GetComponent<TInterface>();

                if (_component == null)
                {
                    Debug.LogErrorFormat("{0} has no {1} component!", link.name, typeof(TInterface).FullName);
                }
            }
        }
    }
}