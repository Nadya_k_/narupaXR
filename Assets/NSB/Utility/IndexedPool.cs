﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace NSB.Utility
{
    /// <summary>
    /// Tool for pooling GameObjects that share a common Component. Allows control
    /// over how many instances are active and access to them and iteration over
    /// them by numerical index.
    /// 
    /// The purpose of pooling is to avoid creating new instances from scratch when
    /// the number of instances may be fluctuating frequently. 
    /// </summary>
    public class IndexedPool<TInstance>
        where TInstance : Component
    {
        /// <summary>
        /// The function for creating new instances from scratch.
        /// </summary>
        private Func<TInstance> factory;

        /// <summary>
        /// The transform that new instances will be parented to.
        /// </summary>
        private Transform parent;

        /// <summary>
        /// An optional setup function on a new instance after creating it.
        /// </summary>
        private Action<TInstance> InstanceSetup;

        /// <summary>
        /// The store of existing instances, including inactive instances that may
        /// be reused later if necessary.
        /// </summary>
        private List<TInstance> instances = new List<TInstance>();

        /// <summary>
        /// The number of active instances.
        /// </summary>
        public int count { get; private set; }

        /// <summary>
        /// Return the instance as the given index. There are always at least
        /// `count` instances.
        /// </summary>
        public TInstance this[int index]
        {
            get
            {
                return instances[index];
            }
        }

        /// <summary>
        /// Set up an IndexedPool which creates new instances via a provided
        /// factory function, parenting them either to the given parent transform 
        /// or the parent transform of the prefab. Optionally a setup function can 
        /// be provided to call on newly created instances.
        /// </summary>
        public IndexedPool(Func<TInstance> factory,
            Transform parent = null,
            Action<TInstance> InstanceSetup = null)
        {
            this.factory = factory;
            this.parent = parent;
            this.InstanceSetup = InstanceSetup ?? delegate { };
        }

        /// <summary>
        /// Set up an IndexedPool which creates new instances by instantiating
        /// copies of a given prefab, parenting them either to the given parent
        /// transform or the parent transform of the prefab. Optionally a setup
        /// function can be provided to call on newly created instances.
        /// </summary>
        public IndexedPool(TInstance prefab, 
            Transform parent = null,
            Action<TInstance> InstanceSetup = null)
            : this(() => Object.Instantiate(prefab), 
                parent ?? prefab.transform.parent, 
                InstanceSetup)
        {
        }

        /// <summary>
        /// Create a black new instance to use in this pool.
        /// </summary>
        private TInstance CreateInstance()
        {
            var instance = factory();

            instance.transform.SetParent(parent, false);

            InstanceSetup(instance);

            return instance;
        }

        /// <summary>
        /// Ensure an exact number of instances are active, creating new instances
        /// if necessary, otherwise reusing existing instances or hiding surplus
        /// instances.
        /// </summary>
        public void SetActive(int count)
        {
            for (int i = instances.Count; i < count; ++i)
            {
                instances.Add(CreateInstance());
            }

            for (int i = this.count; i < count; ++i)
            {
                instances[i].gameObject.SetActive(true);
            }

            for (int i = count; i < instances.Count; ++i)
            {
                instances[i].gameObject.SetActive(false);
            }

            this.count = count;
        }

        /// <summary>
        /// Iterate each active instance in this pool in order and invoke the
        /// given callback with the index and instance.
        /// </summary>
        public void MapActive(Action<int, TInstance> action)
        {
            for (int i = 0; i < count; ++i)
            {
                action(i, instances[i]);
            }
        }

        /// <summary>
        /// Iterate each instance (including unused) in this pool in order and 
        /// invoke the given callback with the index and instance.
        /// </summary>
        public void MapAll(Action<int, TInstance> action)
        {
            for (int i = 0; i < instances.Count; ++i)
            {
                action(i, instances[i]);
            }
        }
    }
}
