﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using UnityEngine;

namespace NSB.Utility
{
    /// <summary>
    /// Pooler for taking and returning instances of a common GameObject
    /// </summary>
    public class SimplePool<TInstance>
        where TInstance : Component
    {
        /// <summary>
        /// The prefab to clone new instances from
        /// </summary>
        protected TInstance prefab;
 
        /// <summary>
        /// The transform that new instances will be parented to
        /// </summary>
        protected Transform parent;
 
        /// <summary>
        /// Set of instances presently in use
        /// </summary>
        protected IndexedSet<TInstance> instances = new IndexedSet<TInstance>();
 
        /// <summary>
        /// Pool of inactive instances for later reuse
        /// </summary>
        protected List<TInstance> spares = new List<TInstance>();
 
        public SimplePool(TInstance prefab, Transform parent=null)
        {
            this.prefab = prefab;
            this.parent = parent == null ? prefab.transform.parent : parent;
        }
 
        /// <summary>
        /// Create and return a new instance from scratch
        /// </summary>
        protected virtual TInstance CreateNew()
        {
            var instance = UnityEngine.Object.Instantiate(prefab);
 
            instance.transform.SetParent(parent, false);
 
            return instance;
        }
 
        /// <summary>
        /// Return an available unused instance, creating a new one from scratch if
        /// no spares exist
        /// </summary>
        public TInstance TakeInstance()
        {
            TInstance instance;
 
            // if there is a spare instance, use that instead of creating a new one
            if (spares.Count > 0)
            {
                instance = spares[spares.Count - 1];
                spares.RemoveAt(spares.Count - 1);
            }
            else
            {
                instance = CreateNew();
            }
 
            instance.gameObject.SetActive(true);
            instances.Add(instance);
 
            return instance;
        }
 
        /// <summary>
        /// Deactivate the given instance and save it for later reuse
        /// </summary>
        public void ReturnInstance(TInstance instance)
        {
            instance.gameObject.SetActive(false);
            instances.Remove(instance);
            spares.Add(instance);
        }
 
        /// <summary>
        /// Iterate each active instance in this pool in order and invoke the
        /// given callback with the instance.
        /// </summary>
        public void MapActive(Action<TInstance> action)
        {
            for (int i = 0; i < instances.Count; ++i)
            {
                action(instances[i]);
            }
        }
 
        /// <summary>
        /// Iterate each instance (including unused) in this pool in order and 
        /// invoke the given callback with the instance.
        /// </summary>
        public void MapAll(Action<TInstance> action)
        {
            for (int i = 0; i < instances.Count; ++i)
            {
                action(instances[i]);
            }
 
            for (int i = 0; i < spares.Count; ++i)
            {
                action(spares[i]);
            }
        }
    }
}