﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System.Collections;
using System.Collections.Generic;
using Narupa.Client.Playback;
using UnityEngine.Assertions;

namespace NSB.Processing
{
    public class FrameHistory<TFrame> : IFrameHistory<TFrame>
    {
        public FrameHistory(int limit = 4096)
        {
            Resize(limit);
        }

        public void Resize(int limit)
        {
            buffer = new TFrame[limit];

            Reset();
        }

        private TFrame[] buffer;

        private int bufferHead;
        private int bufferTail;

        public int DiscardFramesCount { get; private set; }
        public int FirstFrame { get; private set; }
        public int FinalFrame { get; private set; }

        public void Add(TFrame item)
        {
            AddNewestFrame(item);
        }

        public void Clear()
        {
            Reset();
        }

        public bool Contains(TFrame item)
        {
            throw new System.NotImplementedException();
        }

        public void CopyTo(TFrame[] array, int arrayIndex)
        {
            throw new System.NotImplementedException();
        }

        public bool Remove(TFrame item)
        {
            throw new System.NotImplementedException();
        }

        public int Count
        {
            get
            {
                if (bufferTail == -1)
                {
                    return 0;
                }

                if (bufferHead <= bufferTail)
                {
                    return bufferTail - bufferHead + 1;
                }
                else
                {
                    return bufferTail + 1
                                      + buffer.Length - bufferHead;
                }
            }
        }

        public bool IsReadOnly { get; }

        public bool Full => Count == buffer.Length;

        public int IndexOf(TFrame item)
        {
            throw new System.NotImplementedException();
        }

        public void Insert(int index, TFrame item)
        {
            throw new System.NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new System.NotImplementedException();
        }

        public TFrame this[int frame]
        {
            get
            {
                Assert.IsTrue(bufferTail >= 0, "No frames in history!");
                Assert.IsTrue(frame >= FirstFrame && frame <= FinalFrame, "Frame not within bounds!");

                int index = (bufferHead + frame - FirstFrame) % buffer.Length;

                return buffer[index];
            }
            set { throw new System.NotImplementedException(); }
        }

        public void Reset()
        {
            FirstFrame = -1;
            FinalFrame = -1;

            bufferHead = 0;
            bufferTail = -1;

            DiscardFramesCount = 0;
        }

        public void AddNewestFrame(TFrame frame)
        {
            int next;

            if (FirstFrame == -1)
            {
                FirstFrame = 0;
                next = 0;
            }
            else
            {
                next = (bufferTail + 1) % buffer.Length;

                if (next == bufferHead)
                {
                    bufferHead = (bufferHead + 1) % buffer.Length;
                    FirstFrame += 1;

                    DiscardFramesCount += 1;
                }
            }

            buffer[next] = frame;
            bufferTail = next;
            FinalFrame += 1;
        }

        public IEnumerator<TFrame> GetEnumerator()
        {
            for (int i = FirstFrame; i < FinalFrame; i++)
                yield return buffer[i];
        }

        public override string ToString()
        {
            return string.Format("FrameHistory: {0}-{1} ({2} frames)", FirstFrame, FinalFrame, Count);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
