﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Nano.Client;
using NSB.Utility;

namespace NSB.Processing
{
    public class NSBSelection : IList<int>
    {
        private BitArray contains = new BitArray(32768);
        private IndexedSet<int> indexing = new IndexedSet<int>();

        public static NSBSelection AllFromFrame(NSBFrame frame)
        {
            var selection = new NSBSelection();
            selection.SetSourceAtomCount(frame.AtomCount);

            for (int i = 0; i < frame.AtomCount; ++i)
            {
                selection.Add(i);
            }

            return selection;
        }

        public int SourceAtomCount { get; private set; }

        #region IList

        public int this[int index]
        {
            get { return indexing[index]; }
            set
            {
                int prev = indexing[index];
                contains[prev] = false;
                contains[value] = true;
                indexing[index] = value;
            }
        }

        public int Count => indexing.Count;

        IEnumerator<int> IEnumerable<int>.GetEnumerator() => indexing.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => indexing.GetEnumerator();

        int IList<int>.IndexOf(int id)
        {
            throw new System.NotSupportedException("Indexing of selections is only for iteration convenience!");
        }

        void IList<int>.Insert(int index, int id)
        {
            throw new System.NotSupportedException("Indexing of selections is only for iteration convenience!");
        }

        void IList<int>.RemoveAt(int id)
        {
            throw new System.NotSupportedException("Indexing of selections is only for iteration convenience!");
        }

        bool ICollection<int>.IsReadOnly => true; // not sure the semantics of this but... you should not manually modify this IList!

        void ICollection<int>.CopyTo(int[] array, int arrayIndex) => indexing.CopyTo(array, arrayIndex);

        #endregion IList

        public void SetSourceAtomCount(int count)
        {
            contains.Length = count;

            // clear invalid IDs
            for (int i = SourceAtomCount; i >= count; --i)
            {
                indexing.Remove(i);
            }

            SourceAtomCount = count;
        }

        public void Clear()
        {
            contains.SetAll(false);
            indexing.Clear();
        }

        public void SetAll(bool state)
        {
            indexing.Clear();

            contains.SetAll(state);

            if (state)
            {
                for (int i = 0; i < SourceAtomCount; ++i)
                {
                    Add(i);
                }
            }
        }

        public void Invert()
        {
            if (SourceAtomCount > 0)
            {
                for (int i = 0; i < SourceAtomCount; ++i)
                {
                    Toggle(i);
                }
            }
        }

        public void Add(int id)
        {
            contains[id] = true;
            indexing.Add(id);
        }

        public void RemoveAll()
        {
            var ids = this.ToList();
            foreach (int id in ids)
            {
                Remove(id);
            }
        }

        public void AddRange(ICollection<int> range)
        {
            foreach (int id in range)
            {
                Add(id);
            }
        }

        public bool Remove(int id)
        {
            contains[id] = false;
            return indexing.Remove(id);
        }

        public void RemoveRange(ICollection<int> range)
        {
            foreach (int id in range)
            {
                Remove(id);
            }
        }

        public void Set(int id, bool value)
        {
            contains[id] = value;

            if (value)
            {
                Add(id);
            }
            else
            {
                Remove(id);
            }
        }

        public void Toggle(int id)
        {
            if (contains[id])
            {
                Remove(id);
            }
            else
            {
                Add(id);
            }

            contains[id] = !contains[id];
        }

        public bool Contains(int id)
        {
            return contains[id];
        }

        public bool Contains(BondPair bond)
        {
            return contains[bond.A] && contains[bond.B];
        }

        public bool FilterBonds(NSBFrame frame, 
            List<BondPair> bonds,
            bool clear = true)
        {
            if (clear) bonds.Clear();

            for (int i = 0; i < frame.BondCount; ++i)
            {
                var bond = frame.BondPairs[i];

                if (Contains(bond)) bonds.Add(bond);
            }

            return bonds.Count > 0;
        }
    }
}