﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using NSB.Utility;
using UnityEngine;

namespace NSB.Processing
{
    public class NSBStyle
    {
        public static int _ShaderPalette;

        public delegate T AtomStyleGenerator<T>(NSBFrame frame, int visibleID);

        public NSBSelection HighlightedAtoms;

        public bool PaletteIsValid;
        public Texture2D PaletteTexture;
        public Color32[] PaletteColors = new Color32[256];
        private PaletteState paletteState = new PaletteState();

        public List<Color32> AtomColors = new List<Color32>();
        public List<byte> AtomPaletteIndices = new List<byte>();

        public List<float> AtomRadiuses = new List<float>(); // TODO: do we really need this?
        public string Name;

        static NSBStyle()
        {
            _ShaderPalette = Shader.PropertyToID("_Palette");
        }

        private class PaletteState
        {
            public int count;
            public Color32[] pal2col = new Color32[256];
            public Dictionary<Color32, byte> col2pal = new Dictionary<Color32, byte>();

            public void Clear()
            {
                count = 0;
                col2pal.Clear();
            }

            public byte AddColor(Color32 color)
            {
                byte index;

                if (!col2pal.TryGetValue(color, out index))
                {
                    if (count < 256)
                    {
                        index = (byte)count;
                        pal2col[index] = color;
                        col2pal[color] = index;
                        count += 1;
                    }
                    else // no room left in the palette
                    {
                        index = (byte) count;
                        pal2col[index] = color;
                        col2pal[color] = index;
                    }
                }

                return index;
            }
        }

        public void InitPalette()
        {
            PaletteTexture = PaletteTexture ?? new Texture2D(256, 1, TextureFormat.ARGB32, false);
        }

        public void GeneratePalette()
        {
            InitPalette();

            paletteState.Clear();

            for (int i = 0; i < AtomColors.Count; ++i)
            {
                //paletteState.AddColor(AtomColors[i]);
            }

            PaletteColors = paletteState.pal2col;

            CommitPalette();
        }

        public void CommitPalette()
        {
            PaletteTexture.SetPixels32(PaletteColors);
            PaletteTexture.Apply();
        }

        public void UsePalette(Material material)
        {
            material.SetTexture(_ShaderPalette, PaletteTexture);
        }

        public void GenerateAtomStyle<T>(List<T> output,
            NSBFrame frame,
            NSBSelection selection,
            AtomStyleGenerator<T> generator)
        {
            output.Resize(selection.Count);

            for (int i = 0; i < selection.Count; ++i)
            {
                output[i] = generator(frame, selection[i]);
            }
        }

        public void GenerateAtomStyle<T>(List<T> output,
            NSBFrame frame,
            AtomStyleGenerator<T> generator)
        {
            output.Resize(frame.AtomCount);

            for (int i = 0; i < frame.AtomCount; ++i)
            {
                output[i] = generator(frame, i);
            }
        }
    }
}
