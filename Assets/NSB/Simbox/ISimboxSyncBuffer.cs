﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Client;

namespace NSB.Simbox
{
    public delegate void SimboxSyncBufferUpdate<TData>(ISimboxSyncBuffer<TData> buffer);

    public interface ISimboxSyncBuffer<TData>
    {
        int Count { get; }
        TData[] Data { get; }
        bool IsDirty { get; set; }

        event SimboxSyncBufferUpdate<TData> Updated;

        void Attach(ClientBase client);
        void Detach(ClientBase client);
    }
}