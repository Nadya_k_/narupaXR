﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Nano;
using Nano.Client;
using Nano.Commands;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using Narupa.Client.Network;
using Rug.Osc;
using UnityEngine;

namespace NSB.Simbox
{
    public class Client : ClientBase, IMessageSender, IMessageReceiver
    {
        /// <summary>
        /// Interactions stream.
        /// </summary>
        public readonly IDataStream<Nano.Transport.Variables.Interaction.Interaction> Interactions;

        /// <summary>
        /// Stream for transmitting the local VR device positions to the server.
        /// </summary>
        public readonly IDataStream<VRInteraction> LocalVrDevicePositions;

        /// <summary>
        /// Stream for transmitting the atoms that should be associated with an interaction in the Interactions stream.
        /// </summary>
        public readonly IDataStream<int> InteractionAtoms;

        public Client(ITransportPacketTransmitter transmitter, PacketStatistics packetStatistics, IReporter reporter) : base(transmitter, packetStatistics, reporter, false)
        {
            int maxNumberOfAtoms = 1000000;
            Interactions = OutgoingStreams.DefineStream<Nano.Transport.Variables.Interaction.Interaction>((ushort)StreamID.Interaction, "Interaction",
                VariableType.Byte, Marshal.SizeOf(typeof(Nano.Transport.Variables.Interaction.Interaction)), 8 * 4,
                TransportDataStreamTransmitMode.Continuous);
            LocalVrDevicePositions = OutgoingStreams.DefineStream<VRInteraction>((ushort)StreamID.VRInteraction, "SteamVR Interaction", VariableType.Byte, Marshal.SizeOf(typeof(VRInteraction)), 8 * 4, TransportDataStreamTransmitMode.Continuous);
            InteractionAtoms = OutgoingStreams.DefineStream<int>((ushort)StreamID.InteractionSelectedAtoms,
                "Interaction Selected Atoms", VariableType.Int32, 1, maxNumberOfAtoms,
                TransportDataStreamTransmitMode.Continuous);
        }

        protected override IncomingDataAgent CreateDataReader()
        {
            return new ContextIncomingDataAgent(TransportContext);
        }

        public override void Start()
        {
            base.Start();
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        internal void RequestStreamList()
        {
            IncomingStreams.RequestStreamList();
        }

        internal void RequestLeaveAllStreams()
        {
            foreach (ushort id in new List<ushort>(IncomingStreams.Keys))
            {
                IncomingStreams.LeaveStream(id);
            }
        }

        internal void RequestJoinAllStreams()
        {
            foreach (ushort id in new List<ushort>(IncomingStreams.Keys))
            {
                IncomingStreams.JoinStream(id);
            }
        }

        /// <summary>
        /// Send Restart Command.
        /// </summary>
        public void SendRestart()
        {
            SendMessage(SimboxCommands.GetAddress(SimboxCommand.Restart));
        }

        /// <summary>
        /// Send Pause Command
        /// </summary>
        public void SendPause()
        {
            SendMessage(SimboxCommands.GetAddress(SimboxCommand.Pause));
        }

        /// <summary>
        /// Send Play Command.
        /// </summary>
        public void SendPlay()
        {
            SendMessage(SimboxCommands.GetAddress(SimboxCommand.Play));
        }

        /// <summary>
        /// Send Step Command.
        /// </summary>
        public void SendStep()
        {
            SendMessage(SimboxCommands.GetAddress(SimboxCommand.Step));
        }

        /// <summary>
        /// Sends a simulation path for the server to load. 
        /// </summary>
        /// <remarks>
        /// The path is the file location on the server where the simulation XML exists. 
        /// </remarks>
        /// <param name="config"></param>
        public void SendSimulationPath(string config)
        {
            SendMessage("/server/sendSimulationPath", config);
        }

        public void SendMessage(string address, params object[] arguments)
        {
            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(address, arguments));
        }

        public void SubscribeMessage(string address, Action<string, object[]> callback)
        {
            TransportContext.OscManager.Attach(address, message => callback(message.Address, message.ToArray()));
        }
        
        public void UnsubscribeMessage(string address, Action<string, object[]> callback)
        {
            TransportContext.OscManager.Detach(address, message => callback(message.Address, message.ToArray()));
        }
    }
}