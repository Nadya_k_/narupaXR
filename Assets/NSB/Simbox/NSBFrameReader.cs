﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Nano;
using Nano.Client;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using Narupa.VR.Multiplayer;
using NSB.Processing;
using NSB.Simbox.Debugging;
using Rug.Osc;
using UnityEngine;
using Action = System.Action;
using VariableName = Nano.Transport.Variables.VariableName;

namespace NSB.Simbox
{
    /// <summary>
    /// Big blob of code that understands simbox output for both recordings and
    /// live simulations and translates into a FrameHistory for use by the frontend
    /// </summary>
    public class NSBFrameReader
    {

    
    }
}