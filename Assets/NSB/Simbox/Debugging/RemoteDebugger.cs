﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Net;
using Rug.Osc;
using UnityEngine;

namespace NSB.Simbox.Debugging
{
    public class RemoteDebugger : MonoBehaviour
    {
        static OscSender sender;

        public static void Log(string message)
        {
            if (sender == null)
            {
                return; 
            }

            sender.Send(new OscMessage("/log", message)); 
        }

        void Awake()
        {
            sender = new OscSender(IPAddress.Loopback, 8000);
            sender.Connect(); 
        }

        void OnDestroy()
        {
            sender.Close();
            sender = null; 
        }
    }
}
