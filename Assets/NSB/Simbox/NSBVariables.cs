﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Variables;
using SlimMath;
using UnityEngine;

namespace NSB.Simbox
{
    public static partial class NSBVariableExtensions
    {
        public static T GetValue<T>(this IVariable variable)
        {
            return (variable as IVariable<T>).Value;
        }

        public static object GetValue(this IVariable variable)
        {
            if (variable.VariableType == VariableType.Float)
            {
                return variable.GetValue<float>();
            }
            else if (variable.VariableType == VariableType.Byte)
            {
                return variable.GetValue<byte>();
            }
            else if (variable.VariableType == VariableType.Int16)
            {
                return variable.GetValue<System.Int16>();
            }
            else if (variable.VariableType == VariableType.Int32)
            {
                return variable.GetValue<System.Int32>();
            }
            else if (variable.VariableType == VariableType.Int64)
            {
                return variable.GetValue<System.Int64>();
            }
            else if (variable.VariableType == VariableType.UInt16)
            {
                return variable.GetValue<System.UInt16>();
            }
            else if (variable.VariableType == VariableType.UInt32)
            {
                return variable.GetValue<System.UInt32>();
            }
            else if (variable.VariableType == VariableType.UInt64)
            {
                return variable.GetValue<System.UInt64>();
            }
            else if (variable.VariableType == VariableType.String)
            {
                return variable.GetValue<string>();
            }
            else if (variable.VariableType == VariableType.Bool)
            {
                return variable.GetValue<bool>();
            }
            else if (variable.VariableType == VariableType.Vector2)
            {
                return variable.GetValue<UnityEngine.Vector2>();
            }
            else if (variable.VariableType == VariableType.Vector3)
            {
                return variable.GetValue<UnityEngine.Vector3>();
            }
            else if (variable.VariableType == VariableType.BoundingBox)
            {
                return variable.GetValue<BoundingBox>();
            }
            else
            {
                Debug.LogFormat("Ignoring {0}", variable.VariableType);

                return 0;
            }
        }
    }

    public static class NSBVariables
    {
        public static NSBVariable<float> Temperature = new NSBVariable<float>(VariableName.Temperature);
        public static NSBVariable<float> Pressure = new NSBVariable<float>(VariableName.Pressure);
        public static NSBVariable<BoundingBox> SimBoundingBox = new NSBVariable<BoundingBox>(VariableName.SimBoundingBox);
    }

    public class NSBVariable<TValue>
    {
        public readonly VariableName VariableName;

        public NSBVariable(VariableName VariableName)
        {
            this.VariableName = VariableName;
        }

        public IVariable<TValue> GetVariable(VariableCollection variables)
        {
            return variables[VariableName] as IVariable<TValue>;
        }
    }
}