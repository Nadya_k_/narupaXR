﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.IO;
using NSB.Examples.Utility;
using UnityEngine;
using Exception = System.Exception;

namespace NSB.Examples.Data
{
    public static class Files
    {
        public static string SettingsRoot => $"{Application.persistentDataPath}/settings";
        public static string SavedLoginFile => $"{SettingsRoot}/login-code.txt";

        public static string CachedDataRoot => $"{Application.persistentDataPath}/cached";
        public static string RecordingsDataRoot => $"{Application.persistentDataPath}/recordings";

        /// <summary>
        /// Save text to given path, return whether or not this was successful. 
        /// Swallows exceptions but logs them.
        /// </summary>
        public static bool Save(string path, string text)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));

                File.WriteAllText(path, text);

                Android.RefreshFile(path);

                return true;
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
                Debug.LogError($"[Files] Couldn't save text to '{path}'");

                return false;
            }
        }

        /// <summary>
        /// Attempt to load text from the given path, returning whether this was
        /// successful or not. Swallows exceptions but logs them.
        /// </summary>
        public static bool Load(string path, out string text)
        {
            try
            {
                text = File.ReadAllText(path);

                return true;
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
                Debug.LogError($"[Files] Couldn't load text from '{path}'");
                text = default(string);

                return false;
            }
        }

        /// <summary>
        /// Save bytes to given path, return whether or not this was successful. 
        /// Swallows exceptions but logs them.
        /// </summary>
        public static bool Save(string path, byte[] bytes)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));

                File.WriteAllBytes(path, bytes);

                Android.RefreshFile(path);

                return true;
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
                Debug.LogError($"[Files] Couldn't save bytes to '{path}'");

                return false;
            }
        }

        /// <summary>
        /// Attempt to load bytes from the given path, returning whether this was
        /// successful or not. Swallows exceptions but logs them.
        /// </summary>
        public static bool Load(string path, out byte[] bytes)
        {
            try
            {
                bytes = File.ReadAllBytes(path);

                return true;
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
                Debug.LogError($"[Files] Couldn't load bytes from '{path}'");
                bytes = default(byte[]);

                return false;
            }
        }

        /// <summary>
        /// Serialize data to JSON and save to given path, return whether or not
        /// this was successful. Swallows exceptions but logs them.
        /// </summary>
        public static bool Save<TData>(string path, TData data)
        {
            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));

                File.WriteAllText(path, JSON.Serialize(data));

                Android.RefreshFile(path);

                return true;
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
                Debug.LogError($"[Files] Couldn't save {typeof(TData).FullName} to '{path}'");

                return false;
            }
        }

        /// <summary>
        /// Attempt to deserialize JSON data from the given path, returning whether 
        /// this was successful or not. Swallows exceptions but logs them.
        /// </summary>
        public static bool Load<TData>(string path, out TData data)
        {
            try
            {
                string json = File.ReadAllText(path);
                data = JSON.Deserialize<TData>(json);

                return true;
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
                Debug.LogError($"[Files] Couldn't load {typeof(TData).FullName} from '{path}'");
                data = default(TData);

                return false;
            }
        }

        public static bool Delete(string path)
        {
            try
            {
                File.Delete(path);

                Android.RefreshFile(path);

                return true;
            }
            catch (Exception exception)
            {
                Debug.LogException(exception);
                Debug.LogError($"[Files] Couldn't delete '{path}'");

                return false;
            }
        }

        public static string GetChecksum(string file)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            using (FileStream stream = File.OpenRead(file))
            {
                return Convert.ToBase64String(md5.ComputeHash(stream));
            }
        }
    }
}
