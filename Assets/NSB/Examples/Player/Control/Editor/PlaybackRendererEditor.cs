﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using NSB.Simbox;
using UnityEditor;
using UnityEngine;

namespace NSB.Examples.Player.Control.Editor
{
    [CustomEditor(typeof(PlaybackRenderer))]
    public class PlaybackRendererEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            /*
            var player = target as PlaybackRenderer;

            base.OnInspectorGUI();

            if (player.Reader == null || !player.Reader.Ready)
            {
                return;
            }

            if (player.Reader.Networked)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Live Frame", EditorStyles.boldLabel);

                foreach (var pair in player.Reader.Client.VariablePortal.Variables)
                {
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField(pair.Key.Name);
                    EditorGUILayout.LabelField(pair.Value.GetValue().ToString());

                    EditorGUILayout.EndHorizontal();
                }

                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Alter Live Variables", EditorStyles.boldLabel);

                foreach (var pair in player.Reader.Client.VariablePortal.Variables)
                {
                    if (player.Reader.GetVariable(pair.Key).Readonly)
                    {
                        continue;
                    }

                    var type = pair.Value.VariableType;

                    EditorGUILayout.BeginHorizontal();
                
                    if (type == Nano.Transport.Variables.VariableType.Float)
                    {
                        var variable = player.Reader.GetVariable<float>(pair.Key);
                        variable.DesiredValue = EditorGUILayout.FloatField(pair.Key.Name, variable.DesiredValue);
                    }
                    else if (type == Nano.Transport.Variables.VariableType.BoundingBox)
                    {
                        var variable = player.Reader.GetVariable<SlimMath.BoundingBox>(pair.Key);
                        var desired = variable.DesiredValue;

                        desired.Minimum = Convert(EditorGUILayout.Vector3Field("Min", Convert(desired.Minimum)));
                        EditorGUILayout.EndHorizontal();
                        EditorGUILayout.BeginHorizontal();
                        desired.Maximum = Convert(EditorGUILayout.Vector3Field("Max", Convert(desired.Maximum)));

                        variable.DesiredValue = desired;
                    }

                    EditorGUILayout.EndHorizontal();
                }
            }
            else
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Interpolated Frame", EditorStyles.boldLabel);

                foreach (var pair in player.InterpolatedFrame.Variables)
                {
                    EditorGUILayout.BeginHorizontal();

                    EditorGUILayout.LabelField(pair.Key.ToString());
                    EditorGUILayout.LabelField(pair.Value.ToString());

                    EditorGUILayout.EndHorizontal();
                }
            }

            //EditorGUILayout.HelpBox(builder.ToString(), MessageType.None);

            Repaint();*/
        }

        public static Vector3 Convert(SlimMath.Vector3 vector)
        {
            return new Vector3(vector.X, vector.Y, vector.Z);
        }

        public static SlimMath.Vector3 Convert(Vector3 vector)
        {
            return new SlimMath.Vector3(vector.x, vector.y, vector.z);
        }
    }
}
