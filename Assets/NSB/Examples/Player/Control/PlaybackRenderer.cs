﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Client;
using Nano.Rainbow.v1_0;
using NSB.Debugging;
using NSB.Interaction;
using NSB.MMD;
using NSB.MMD.RenderingTypes;
using NSB.MMD.Renders;
using NSB.Processing;
using NSB.Simbox;
using NSB.Simbox.Debugging;
using UnityEngine;
using UnityEngine.Profiling;
using UnityEngine.UI;

namespace NSB.Examples.Player.Control
{
    public class PlaybackRenderer : MonoBehaviour, IFrameSource
    {
        [Header("Loading Icon")]
        [SerializeField]
        private GameObject loadingObject;

        [SerializeField]
        private Image loadingImage;

        [Header("Camera")]
        public new Camera camera;

        [SerializeField]
        private Transform cameraPivot;

        [Header("Interaction")]
        [SerializeField]
        private RaycastFrameAtoms picking;

        [Header("Rendering")]
        [SerializeField]
        private NSBRepresentation NSBRenderer;
        [SerializeField]
        private BoundingBoxRenderer boundsRenderer;
        [SerializeField]
        private TransformFollowAtomCentroid cameraFollow;
        [SerializeField]
        private RaycastFrameAtoms raycastFrameAtoms;

        public InspectorSelection VisibleAtoms;
        public InspectorSelection SelectedAtoms;
        public InspectorSelection FrameSelectedAtoms;

        public bool Looping = true;
        public float PlaybackFPS = 30;
        public bool CenterCamera = true;

        public NSBFrameReader Reader;

        public NSBFrame InterpolatedFrame = new NSBFrame();

        public FrameHistory<NSBFrame> history { get; private set; }
        public float CurrentFrame { get; private set; }
        private NSBFrame prevFrame;
        private NSBFrame nextFrame;

        private bool Ready;

        public System.Action OnReady = delegate { };

        NSBFrame IFrameSource.Frame
        {
            get
            {
                return InterpolatedFrame;
            }
        }

        private void Awake()
        {
            Clear();
        }

        public void SetFat()
        {
            NSBRenderer.Settings.Mode.Value = 2;
        }

        public void SetThin()
        {
            NSBRenderer.Settings.Mode.Value = 1;
        }
    
        public void SetNone()
        {
            NSBRenderer.Settings.Mode.Value = 0;
        }

        private SimulationMetadata loadingSim;
        private string loadingSim2;

        public void EnterRecording(string path)
        {
            Clear();
/*
            Reader.PlayFromFile(path);*/

            loadingObject.SetActive(false);
        }

        public void EnterRecording(byte[] bytes)
        {/*
            Clear();

            Reader.PlayFromBytes(bytes);*/

            loadingObject.SetActive(false);
        }

        public void EnterServer(SimboxConnectionInfo info)
        {
            loadingObject.SetActive(true);
/*
            Reader.TryEnterServer(info,
                OnSuccess: () => { Reader.Client.SendPlay(); loadingObject.SetActive(false); },
                OnFailure: () => loadingObject.SetActive(false));*/
        }

        private void OnRecordingReady(string path)
        {
            loadingObject.SetActive(false);
            EnterRecording(path);
        }

        private void OnDownloadProgress(float progress)
        {
            loadingImage.fillAmount = progress;
        }

        public void Clear()
        {
            /*
            Reader = Reader ?? new NSBFrameReader();

            history = Reader.history;

            loadingObject.SetActive(false);
            Ready = false;
            Reader.Reset();
            Reader.history.Reset();
            CurrentFrame = 0;
            NSBFrame.Clear(InterpolatedFrame);
            VisibleAtoms.Selection.Clear();
            SelectedAtoms.Selection.Clear();

            boundsRenderer.gameObject.SetActive(false);
            */
        }

        public bool PickAtom(Vector2 screen, out int id)
        {
            /*if (!Reader.Ready)
            {
                id = 0;
                return false;
            }
*/
            return picking.RaycastScreenPoint(screen, out id, camera);
        }

        private void Update()
        {
            /*if (!Reader.Ready)
            {
                Ready = false;
            }

            ConsoleReporter.LogToUnityConsole = false;

            Reader.Update();

            if (Reader.history.Count > 0)
            {
                float nextFrame = CurrentFrame + Time.deltaTime * PlaybackFPS;

                if (Looping && !Reader.Networked && Reader.Complete)
                {
                    if (nextFrame > Reader.history.FinalFrame)
                    {
                        nextFrame = Reader.history.FirstFrame;
                    }
                    else if (nextFrame < Reader.history.FirstFrame)
                    {
                        nextFrame = Reader.history.FinalFrame;
                    }
                }
                else
                {
                    if (nextFrame > Reader.history.FinalFrame)
                    {
                        nextFrame = Reader.history.FinalFrame;
                    }
                    else if (nextFrame < Reader.history.FirstFrame)
                    {
                        nextFrame = Reader.history.FirstFrame;
                    }
                }

                SetFrame(nextFrame);
            }*/
        }

        public void SetFrame(float frame)
        {
            if (history == null)
                return;

            Profiler.BeginSample("Gather Playback Frame");

            lock (history)
            {
                CurrentFrame = Mathf.Clamp(frame,
                    history.FirstFrame,
                    history.FinalFrame);

                if (history.Count == 0)
                {
                    return;
                }

                int prev = Mathf.Clamp(Mathf.FloorToInt(CurrentFrame), history.FirstFrame, history.FinalFrame);
                int next = Mathf.Clamp(Mathf.CeilToInt(CurrentFrame), history.FirstFrame, history.FinalFrame);

                prevFrame = history[prev];
                nextFrame = history[next];
            }

            Profiler.EndSample();

            Refresh();
        }

        public void Refresh()
        {
            Profiler.BeginSample("PlaybackRenderer Refresh");

            NSBFrame.Interpolate(prevFrame, nextFrame, InterpolatedFrame, CurrentFrame % 1);

            VisibleAtoms.Selection.SetSourceAtomCount(InterpolatedFrame.AtomCount);
            SelectedAtoms.Selection.SetSourceAtomCount(InterpolatedFrame.AtomCount);
            FrameSelectedAtoms.Selection.SetSourceAtomCount(InterpolatedFrame.AtomCount);

            if (!Ready)
            {
                Ready = true;
                OnReady();
            }

            // camera filter
            var bounds = prevFrame.GetVariable<SlimMath.BoundingBox>(Nano.Transport.Variables.VariableName.SimBoundingBox);
            var center = (bounds.Minimum + bounds.Maximum) * 0.5f;

            //boundsRenderer.gameObject.SetActive(true);

            if (!CenterCamera) cameraPivot.position = new Vector3(center.X, center.Y, center.Z);
            cameraFollow.enabled = CenterCamera;

            // only > 0 during live simulations?
            if (InterpolatedFrame.SelectedAtomIndicies != null
                && InterpolatedFrame.SelectedAtomIndicies.Length > 0)
            {
                FrameSelectedAtoms.Selection.Clear();

                for (int i = 0; i < InterpolatedFrame.SelectedAtomIndicies.Length; ++i)
                {
                    FrameSelectedAtoms.Selection.Add(InterpolatedFrame.SelectedAtomIndicies[i]);
                }
            }
            else if (InterpolatedFrame.SelectedAtomIndicies != null)
            {
                FrameSelectedAtoms.Selection.Clear();
            }

            Profiler.EndSample();
        }

        public float GetMargin()
        {
            return 128f / 1920f;
        }

        public void SetLeft()
        {
            float margin = GetMargin();
            float width = (1 - margin) / 2f;

            camera.rect = new Rect(margin, 0, width, 1f);
        }

        public void SetRight()
        {
            float margin = GetMargin();
            float width = (1 - margin) / 2f;

            camera.rect = new Rect(margin + width, 0, width, 1f);
        }

        public void SetFull()
        {
            camera.rect = new Rect(0, 0, 1, 1);
        }

        private bool building;

        private void OnDestroy()
        {
            //Reader.Reset();
        }

        public void MediaPlay()
        {
            //Reader.Client.SendPlay();
        }

        public void MediaPause()
        {
            //Reader.Client.SendPause();
        }

        public void MediaStep()
        {
            //Reader.Client.SendStep();
        }

        public void MediaReset()
        {
            //Reader.Client.SendRestart();
        }
    }
}