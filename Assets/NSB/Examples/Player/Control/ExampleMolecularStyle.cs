﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using Nano.Science;
using NSB.Utility;
using UnityEngine;

// something to recompute the desired element colours / radiuses
namespace NSB.Examples.Player.Control
{
    public static class ExampleMolecularStyle
    {
        private static float saturationMultiplier = 0.65f;
        private static Color colorMultiplier = Color.white;

        public static Color32[] ElementColors;
        public static Color32[] ElementColorsBonds;
        public static float[] ElementRadiuses;

        static ExampleMolecularStyle()
        {
            int length = (int) Element.MAX + 1;

            ElementColors = new Color32[length];
            ElementColorsBonds = new Color32[length];
            ElementRadiuses = new float[length];

            RefreshElementStyles();
        }

        public static void RefreshElementStyles()
        {
            foreach (var value in System.Enum.GetValues(typeof(Element)))
            {
                var element = (Element) value;

                if (element == Element.MAX)
                    break;

                float h, s, v;

                var properties = PeriodicTable.GetElementProperties(element);
                var color = properties.CPKColor.ParseHexColor();

                color *= colorMultiplier;

                Color.RGBToHSV(color, out h, out s, out v);

                ElementColors[(int) element] = Color.HSVToRGB(h, s * saturationMultiplier, v);
                ElementColorsBonds[(int) element] = Color.HSVToRGB(h, s * (saturationMultiplier + 1) * 0.5f, v * v);
                ElementRadiuses[(int) element] = properties.VDWRadius;
            }
        }
    }
}
