﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Examples.UGUI
{
    public class AnimatorToggleBool : MonoBehaviour
    {
        [SerializeField]
        private Animator animator;
        [SerializeField]
        private string parameter;

        public void Set(bool value)
        {
            animator.SetBool(parameter, value);
        }

        public void Toggle()
        {
            bool open = !animator.GetBool(parameter);

            animator.SetBool(parameter, open);
        }
    }
}
