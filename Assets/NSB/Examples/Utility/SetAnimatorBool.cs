﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;

namespace NSB.Examples.Utility
{
    public class SetAnimatorBool : MonoBehaviour 
    {
        [SerializeField]
        private Animator animator;
        [SerializeField]
        private string parameter;
        [SerializeField]
        private bool value;

        public void Invoke()
        {
            animator.SetBool(parameter, value);
        }
    }
}
