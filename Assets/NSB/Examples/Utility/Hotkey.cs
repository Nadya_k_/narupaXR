﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using UnityEngine;
using UnityEngine.Events;

namespace NSB.Examples.Utility
{
    public class Hotkey : MonoBehaviour 
    {
        [System.Serializable]
        public class OnPressed : UnityEvent { }

        public KeyCode key;
        public OnPressed onPressed;

        private void Update()
        {
            if (Input.GetKey(key))
            {
                onPressed.Invoke();
            }
        }
    }
}
