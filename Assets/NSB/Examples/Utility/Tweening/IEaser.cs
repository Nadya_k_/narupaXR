﻿namespace NSB.Examples.Utility.Tweening
{
    enum EaseMode { In, Out, InOut };

    interface IEaser
    {
		float EaseIn(float ratio);
		
		float EaseOut(float ratio);
		
		float EaseInOut(float ratio);
    }
}
