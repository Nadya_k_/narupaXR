﻿using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;

public class PhysicallyCalibratedSpaceTests
{
    [Test, Combinatorial]
    public void CalibratedMatrix_AgreesWithNestedTransforms(
        [ValueSource(typeof(TransformTestData), nameof(TransformTestData.CalibrationPointPairs))] CalibrationPointPair calibration)
    {
        var transform = new GameObject("Test Transform").transform;
        transform.position = calibration.Point1;
        transform.LookAt(calibration.Point2, Vector3.up);

        var space = new PhysicallyCalibratedSpace();
        space.CalibrateWithTwoPoints(calibration.Point1, calibration.Point2);

        var matrix1 = transform.localToWorldMatrix;

        space.CopyToUnityTransform(transform);

        var matrix2 = transform.localToWorldMatrix;

        TestAsserts.AreApproximatelyEqual(matrix1, matrix2);
    }
    
    [Test, Combinatorial]
    public void TransformRotations_WithArbitaryRotations_AgreesWithNestedTransforms(
        [ValueSource(typeof(TransformTestData), nameof(TransformTestData.CalibrationPointPairs))] CalibrationPointPair calibration,
        [ValueSource(typeof(TransformTestData), nameof(TransformTestData.Rotations))] Quaternion rotation)
    {
        var transform = new GameObject("Test Transform").transform;
        transform.position = calibration.Point1;
        transform.LookAt(calibration.Point2, Vector3.up);

        var space = new PhysicallyCalibratedSpace();
        space.CalibrateWithTwoPoints(calibration.Point1, calibration.Point2);

        var testChild1 = new GameObject("Test Child 1").transform;
        testChild1.SetParent(transform, true);

        var testChild2 = new GameObject("Test Child 2").transform;
        testChild2.SetParent(transform, true);

        testChild1.rotation = rotation;
        testChild2.localRotation = space.TransformRotationWorldToLocal(testChild1.rotation);

        TestAsserts.AreApproximatelyEqual(testChild1.localToWorldMatrix, 
                                          testChild2.localToWorldMatrix);
    }

    [Test, Pairwise]
    public void PointTransformsWorldToLocalToWorld_WithRandomPoints_GiveOriginalPoints(
        [ValueSource(typeof(TransformTestData), nameof(TransformTestData.CalibrationPointPairs))] CalibrationPointPair calibration,
        [ValueSource(typeof(TransformTestData), nameof(TransformTestData.TrackingPoints))] Vector3 point)
    {
        var space = new PhysicallyCalibratedSpace();
        space.CalibrateWithTwoPoints(calibration.Point1, calibration.Point2);
        
        var prev = point;
        var next = space.TransformPointLocalToWorld(space.TransformPointWorldToLocal(prev));

        TestAsserts.AreApproximatelyEqual(prev, next);
    }

    [Test, Combinatorial]
    public void FlippedPositions(
        [ValueSource(typeof(TransformTestData), nameof(TransformTestData.CalibrationPointPairs))] CalibrationPointPair calibration)
    {
        var space1 = new PhysicallyCalibratedSpace();
        space1.CalibrateWithTwoPoints(calibration.Point1, calibration.Point2);
        var space2 = new PhysicallyCalibratedSpace();
        space2.CalibrateWithTwoPoints(calibration.Point2, calibration.Point1);

        var forward1 = (calibration.Point2 - calibration.Point1).normalized;
        var forward2 = (calibration.Point1 - calibration.Point2).normalized;

        var point1 = calibration.Point1 + forward1;
        var point2 = calibration.Point2 + forward2;

        var local1 = (space1.TransformPointWorldToLocal(point1));
        var local2 = (space2.TransformPointWorldToLocal(point2));
        
        TestAsserts.AreApproximatelyEqual(local1, local2);
    }
}
