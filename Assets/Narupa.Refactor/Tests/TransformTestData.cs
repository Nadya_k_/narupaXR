﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Random = UnityEngine.Random;

public struct CalibrationPointPair
{
    public Vector3 Point1, Point2;

    public CalibrationPointPair(Vector3 point1, Vector3 point2)
    {
        Point1 = point1;
        Point2 = point2;
    }
};

public struct ControlPointTRSPair
{
    public Matrix4x4 LeftController;
    public Matrix4x4 RightController;
}

public struct TwoPointGestureTestSequence
{
    public Matrix4x4 InitialObjectMatrix;
    public ControlPointTRSPair[] ControlPointsSequence;
}

public static class TransformTestData
{
    private static Matrix4x4 GetRandomTRSMatrix(Vector3? translation = null,
                                                Quaternion? rotation = null,
                                                Vector3? scale = null)
    {
        return Matrix4x4.TRS(translation ?? Random.onUnitSphere, 
                             rotation ?? Random.rotation, 
                             scale ?? Vector3.one * Random.value);
    }

    private static Matrix4x4 GetRandomControlPoint()
    {
        return GetRandomTRSMatrix(scale: Vector3.one);
    }

    public static int[] RandomSeeds =
    {
        6271, 2781, 27269, 1781, 28706, 682, 6486, 18894, 5004, 32750, 23851, 9350, 36173, 40423, 16888, 60834,
    };

    public static IEnumerable<TwoPointGestureTestSequence> TwoPointGestureSequences
    {
        get
        {
            foreach (int seed in RandomSeeds)
            {
                Random.InitState(seed);

                yield return new TwoPointGestureTestSequence
                {
                    InitialObjectMatrix = GetRandomTRSMatrix(),
                    ControlPointsSequence = Enumerable.Range(0, 16).Select(_ =>
                    {
                        return new ControlPointTRSPair
                        {
                            LeftController = GetRandomControlPoint(),
                            RightController = GetRandomControlPoint(),
                        };
                    }).ToArray(),
                };
            }
        }
    }

    public static CalibrationPointPair[] CalibrationPointPairs =
    {
        new CalibrationPointPair(new Vector3( 0, 10,  0), new Vector3( 10,  11,  10)),
        new CalibrationPointPair(new Vector3(-1, 10, -3), new Vector3(100, 110, 100)),
    };

    public static Vector3[] TrackingPoints =
    {
        new Vector3(6.80330299858f, 1.81320831628f, -5.68456782133f),
        new Vector3(1.08367728622f, -7.73913562372f, 2.50166775384f),
        new Vector3(6.25929806268f, -5.12218775497f, -8.5314947984f),
        new Vector3(0.314473785737f, -1.35953457049f, 2.37481572783f),
        new Vector3(-5.97038800641f, -2.72064297786f, 7.35998598337f),
        new Vector3(-2.50755623482f, 0.306140611828f, -2.23709854751f),
        new Vector3(-2.58585165755f, -9.85241090195f, 9.49086006626f),
        new Vector3(-0.392104907632f, -9.07240379286f, -0.252825118491f),
        new Vector3(-2.66917629762f, 5.20481768073f, 9.4540261558f),
        new Vector3(4.57149577439f, -0.505946156524f, -6.00587831588f),
        new Vector3(2.10333593152f, 9.67380435461f, 9.4683210165f),
        new Vector3(-4.66861638788f, 2.85413507525f, -4.92410155573f),
        new Vector3(-4.24578614394f, -6.12363940425f, 3.93497021163f),
        new Vector3(6.10613073299f, 2.51049007215f, -6.09510282023f),
        new Vector3(-3.94889303737f, -1.70155876657f, 4.86202577224f),
        new Vector3(-5.61294594137f, -8.02209876551f, -4.99401350522f),
    };

    public static Quaternion[] Rotations =
    {
        new Quaternion(0.05611993f, -0.1656272f, -0.5912078f, 0.7873319f),
        new Quaternion(-0.5212376f, -0.7140388f, -0.1863066f, 0.4286605f),
        new Quaternion(0.5877193f, -0.7464013f, -0.1284537f, 0.2845538f),
        new Quaternion(0.7412462f, 0.3264454f, 0.3236498f, 0.48912f),
        new Quaternion(0.4562042f, 0.6173159f, -0.1561915f, 0.6216132f),
        new Quaternion(0.8269444f, 0.105981f, -0.3122469f, 0.4554482f),
        new Quaternion(-0.7050962f, 0.5285161f, 0.2765284f, 0.3834606f),
        new Quaternion(-0.2706757f, 0.4309488f, 0.1069537f, 0.8541538f),
        new Quaternion(0.6552219f, 0.6207034f, 0.4228627f, 0.08123185f),
        new Quaternion(0.01473632f, 0.9642773f, 0.2630061f, 0.02792923f),
        new Quaternion(0.3313456f, 0.3683503f, -0.000942148f, 0.8686353f),
        new Quaternion(0.2723727f, -0.3430174f, -0.274754f, 0.855957f),
        new Quaternion(-0.3223152f, -0.9382542f, -0.1219105f, 0.03049143f),
        new Quaternion(-0.1658081f, -0.0690212f, 0.3669414f, 0.912742f),
        new Quaternion(-0.2398205f, -0.1018436f, 0.0846004f, 0.9617468f),
        new Quaternion(-0.4507591f, 0.8619478f, -0.1888226f, 0.1349379f),
    };

    public static float[] UniformScales =
    {
        1.82721054819f, 1.26885357882f, 2.77647350185f, 2.76574246811f, 2.84432265698f, 1.45943880412f, 2.91376547834f, 1.84083976074f, 1.61228803655f, 0.770261613187f, 0.702938998641f, 2.3407763588f, 2.98725715118f, 1.19748306444f, 0.13949565448f, 0.568185729769f,
    };
}
